# Platform V2 Graph Based Domain Structure

## Key nodes within the system
The application exists to track injuries and performance of Athletes, as such they are the core node within the domain. Athletes are organized into Rosters, each Roster having a title and defined starting and ending date. Rosters relate to Teams and Sports. To further track Athlete's relationships to a Roster the PLAYS_ON relationship itself has a start and end date as well (manages the mid-season trade). 

## Security/Health Access Model
### Problem statement
There are multiple forms of access to patient health/test data within the overall system. So users have only read access to data because they are not qualified to test administer testing. Others have administrative rights as well as read writes. Others have the rights to clear a player to play again as well as to administer the testing. These same rights can also be assigned as a group as well as to a principal/user within the system.

More over a User within the system could possibly have a variety of roles and manage a different collection of patients in a different capacity. An example is a Coach of a College football team who manages that team roster as a Coach, but also manages his own children as a Parent/Guardian.

### Solution
Users can have a UserManagesAthleteCare relationship with Athletes. This relationship is most appropriate for an private clinician managing an patient for a Parent/Guardian relationship. The relationship has the additional properties of a start/end date, a CareType enumeration and detailed ACL that can be used to verify security relationships (see ACL's below). By embedding the start/end date information in the relationship we can track history and quickly find the current relationship as well.

#### Roles vs. Groups
In this context it's more likely for an individual user/principal to have a specific care relationship with an patient or with a roster of patients than to have a large group of users with the exact same ACL. With that in mind the domain will be simplified to simply track a users Roles with in the system. Each Role will carry a specific ACL unique to that Role. To be assigned a specific relationship with any node in the system the system will verify that the user has a Role with the specific ACL for that relationship.

Group membership will serve as a means of adding and removing roles from member users. For example by joining the clinicians group a User would inherit the Roles assigned to that group. These roles would be immediately added to the User's Roles collection. Being removed from the group would remove these Roles from the User's Roles collection.

To reduce the number of relationships between nodes within the system these rights are managed with a bit based ACL with a values very similar to a UNIX file system permissions structure with the Execute bit representing the ability of a user or group to return a player back to play. To model this within the graph DB structure we use a MANAGES_CARE relationship with a property of ACL - the value of that ACL being used as the bit mask to determine the group or users rights to manage care for the Athlete or group of Athletes. When querying for the list of Athletes a Group or User can manage it's possible to return all of the Athletes that have a *_MANAGES_CARE relationship or filter the query by the ACL property within the relationship.



