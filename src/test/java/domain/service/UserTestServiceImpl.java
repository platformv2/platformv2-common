package domain.service;

import com.x2biosystems.platform.common.domain.Group;
import com.x2biosystems.platform.common.domain.Role;
import com.x2biosystems.platform.common.domain.User;
import domain.repository.GroupRepository;
import domain.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Set;

/**
 * Created by ranbir on 12/26/14.
 */

@Component
public class UserTestServiceImpl implements UserTestService {

    @Autowired
    private UserRepository  userRepository;

    @Autowired
    private GroupRepository groupRepository;

    public User addUserToAGroup(Group group,User user) {
        user.addGroup(group);
        Set<Role> fullRoleSet = userRepository.findAllRolesForAUserByGroups(user.getUsername());
        user.getRoles().clear();
        user.addRole(fullRoleSet);
        userRepository.save(user);
        return user;
    }

    public User removeUserFromAGroup(Group group,User user) {
        user.deleteGroup(group);
        Set<Role> fullRoleSet = userRepository.findAllRolesForAUserByGroups(user.getUsername());
        user.getRoles().clear();
        user.addRole(fullRoleSet);
        userRepository.save(user);
        return user;
    }
}
