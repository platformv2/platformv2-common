package domain.service;

import com.x2biosystems.platform.common.domain.Group;
import com.x2biosystems.platform.common.domain.User;

/**
 * Created by ranbir on 12/26/14.
 */
public interface UserTestService {

    User addUserToAGroup(Group group,User user);
}
