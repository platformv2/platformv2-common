package domain.repository;

import com.x2biosystems.platform.common.domain.Address;
import com.x2biosystems.platform.common.domain.AddressType;
import com.x2biosystems.platform.common.domain.DTO.FullAddress;
import com.x2biosystems.platform.common.domain.Role;
import com.x2biosystems.platform.common.domain.Roster;
import com.x2biosystems.platform.common.domain.Team;
import com.x2biosystems.platform.common.domain.UserAssociatedWithTeam;
import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.neo4j.repository.GraphRepository;

import java.util.Set;

/**
 * Created by  <a href="mailto:ranbir.chawla@14zen.com">Ranbir Chawla</a> on 1/15/15.
 */


public interface TeamRepository extends GraphRepository<Team> {

    Team findByTitle(String title);

    @Query("match (team {title:{0}})-[r:ASSOCIATED_TO_TEAM]-(roster) where roster.startDate < {1} and roster.endDate = 0 return roster")
    Roster getCurrentRoster(String title, Long now);

    @Query("match (user {username:{0}})-[r:USER_ASSOCIATED_TO_TEAM]-(team) where r.startDate < {1} and r.endDate = 0 return team")
    Set<Team> getTeamsAssociatedWithUser(String username,Long now);

    @Query("match (user {username:{0}})-[r:USER_ASSOCIATED_TO_TEAM]-(team) return r")
    UserAssociatedWithTeam getRoleForUserAssociatedWithTeam(String username,Long teamId,Long now);


    @Query("MATCH (team {title:{0}})-[h:TEAM_HAS_ADDRESS]-(address) where h.addressType = {1} return address ")
    Set<Address> findAddressByAddressType(String title,AddressType addressType);

    @Query("MATCH (team {title:{0}})-[h:TEAM_HAS_ADDRESS]-(address)-[:IN_LOCALITY]-(locality)-[:IN_REGION]-(region)-[:IN_COUNTRY]-(country) where h.addressType = {1} " +
            "return address.address1, address.address2, address.address3, address.address4,address.postalCode,locality.title, region.code, country.code")
    Set<FullAddress> findFullAddressByAddressType(String title,AddressType addressType);

}
