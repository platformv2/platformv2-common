package domain.repository;

import com.x2biosystems.platform.common.domain.Address;
import com.x2biosystems.platform.common.domain.AddressType;
import com.x2biosystems.platform.common.domain.Association;
import com.x2biosystems.platform.common.domain.DTO.FullAddress;
import com.x2biosystems.platform.common.domain.Team;
import com.x2biosystems.platform.common.domain.User;
import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.neo4j.repository.GraphRepository;

import java.util.List;
import java.util.Set;

/**
 * Created by  <a href="mailto:ranbir.chawla@14zen.com">Ranbir Chawla</a> on 1/16/15.
 */

public interface AssociationRepository extends GraphRepository<Association> {

    Association findById(Long id);
    Association findByTitle(String title);

    @Query("match (user {username:{0}})-[m:USER_MANAGES_ASSOCIATION]-(association) where m.startDate < {1} return association")
    Association getAssociationRelatedToTeamUser(String username,Long now);

    @Query("match (association {title:{0}}) - [m:PLAYS_FOR] - (team) return team")
    List<Team> getTeamsByTitle(String title);

    @Query("match (association {title:{0}}) - [m:USER_MANAGES_ASSOCIATION] - (user) where m.startDate < {1} return user")
    List<User> findAssociationManagers(String title,Long now);

    @Query("MATCH (association {title:{0}})-[h:ASSOCIATION_HAS_ADDRESS]-(address) where h.addressType = {1} return address ")
    Set<Address> findAddressByAddressType(String title,AddressType addressType);

    @Query("MATCH (association {title:{0}})-[h:ASSOCIATION_HAS_ADDRESS]-(address)-[:IN_LOCALITY]-(locality)-[:IN_REGION]-(region)-[:IN_COUNTRY]-(country) where h.addressType = {1} " +
            "return address.address1, address.address2, address.address3, address.address4,address.postalCode,locality.title, region.code, country.code")
    Set<FullAddress> findFullAddressByAddressType(String title,AddressType addressType);
}
