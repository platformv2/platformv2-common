package domain.repository;

import com.x2biosystems.platform.common.domain.Patient;
import com.x2biosystems.platform.common.domain.Roster;
import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.neo4j.repository.GraphRepository;

import java.util.Set;

/**
 * Created by ranbir on 12/25/14.
 */
public interface RosterRepository extends GraphRepository<Roster> {

    @Query("match (roster {title:{0}})-[p:PLAYS_ON]-(Patient) where (p.startDate >= {1} AND p.endDate < {2}) and " +
            "(roster.startDate >= {1} and roster.endDate < {2}) return Patient")
    Set<Patient> getPatientsByTitleAndDateRange(String title,Long startDateMillis,Long endDateMillis);

    Roster       findByTitle(String title);
}
