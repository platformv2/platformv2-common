package domain.repository;

import com.x2biosystems.platform.common.domain.Sport;
import org.springframework.data.domain.Sort;
import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.neo4j.repository.GraphRepository;

import java.util.List;

/**
 * Created by ranbir on 12/24/14.
 */
public interface SportRepository extends GraphRepository<Sport> {

    @Query("match (sport) return sport order by sport.displayOrder ASC")
    List<Sport> findAllSportsOrderByDisplayOrder();
}
