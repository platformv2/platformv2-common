package domain.repository;

import com.x2biosystems.platform.common.domain.Patient;
import com.x2biosystems.platform.common.domain.Gender;
import org.springframework.data.neo4j.repository.GraphRepository;

import java.util.Set;


/**
 * Created by ranbir on 12/24/14.
 */

public interface PatientRepository extends GraphRepository<Patient> {

    Patient findByName(String name);
    Set<Patient> findByGender(Gender gender);
}
