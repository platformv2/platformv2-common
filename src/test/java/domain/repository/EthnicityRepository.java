package domain.repository;

import com.x2biosystems.platform.common.domain.Ethnicity;
import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.neo4j.repository.GraphRepository;

import java.util.List;

/**
 * Created by ranbir on 2/3/15.
 */
public interface EthnicityRepository extends GraphRepository<Ethnicity> {

    @Query("match (ethnicity:Ethnicity) return ethnicity order by ethnicity.displayOrder ASC")
    List<Ethnicity> findAllSortByDisplayOrder();
}
