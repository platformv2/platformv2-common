package domain.repository;

import com.x2biosystems.platform.common.domain.Role;
import org.springframework.data.neo4j.repository.GraphRepository;

/**
 * Created by ranbir on 12/25/14.
 */

public interface RoleRepository extends GraphRepository<Role> {
}
