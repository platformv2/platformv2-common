package domain.repository;

import com.x2biosystems.platform.common.domain.Language;
import org.springframework.data.neo4j.repository.GraphRepository;

/**
 * Created by  <a href="mailto:ranbir.chawla@14zen.com">Ranbir Chawla</a> on 1/15/15.
 */

public interface LanguageRepository extends GraphRepository<Language> {

    Language findByLocaleId(String localeId);
}
