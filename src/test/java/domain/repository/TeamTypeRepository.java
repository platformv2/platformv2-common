package domain.repository;

import com.x2biosystems.platform.common.domain.TeamType;
import com.x2biosystems.platform.common.domain.TeamType;
import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.neo4j.repository.GraphRepository;

import java.util.List;

/**
 * Created by ranbir on 2/3/15.
 */

public interface TeamTypeRepository extends GraphRepository<TeamType> {

    @Query("match (teamType:TeamType) return teamType order by teamType.displayOrder ASC")
    List<TeamType> findAllTeamTypesOrderByDisplayOrder();
}
