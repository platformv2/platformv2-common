package domain.repository;

import com.x2biosystems.platform.common.domain.PositionCategory;
import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.neo4j.repository.GraphRepository;

import java.util.List;

/**
 * Created by ranbir on 2/3/15.
 */
public interface PositionCategoryRepository extends GraphRepository<PositionCategory> {

    @Query("match (sport {displayName:{0}}) - [r:POSITION_IN] - (positionCategory) return positionCategory order by positionCategory.displayOrder ASC")
    List<PositionCategory> findAllPositionCategoryForASportByDisplayOrder(String sportTitle);
}
