package domain.repository;

import com.x2biosystems.platform.common.domain.Address;
import org.springframework.data.neo4j.repository.GraphRepository;

/**
 * Created by  <a href="mailto:ranbir.chawla@14zen.com">Ranbir Chawla</a> on 12/27/14.
 */

public interface AddressRepository extends GraphRepository<Address> {


}
