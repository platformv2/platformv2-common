package domain.repository;

import com.x2biosystems.platform.common.domain.AgeLevel;
import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.neo4j.repository.GraphRepository;

import java.util.List;

/**
 * Created by ranbir on 2/4/15.
 */
public interface AgeLevelRepository extends GraphRepository<AgeLevel> {

    @Query("match (ageLevel:AgeLevel) return ageLevel order by ageLevel.displayOrder")
    List<AgeLevel> findAllByDisplayOrder();
}
