package domain.repository;

import com.x2biosystems.platform.common.domain.Address;
import com.x2biosystems.platform.common.domain.AddressType;
import com.x2biosystems.platform.common.domain.Association;
import com.x2biosystems.platform.common.domain.HealthCareFacility;
import com.x2biosystems.platform.common.domain.Patient;
import com.x2biosystems.platform.common.domain.CareType;
import com.x2biosystems.platform.common.domain.DTO.FullAddress;
import com.x2biosystems.platform.common.domain.Role;
import com.x2biosystems.platform.common.domain.User;
import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.neo4j.repository.GraphRepository;

import java.util.Set;

/**
 * Created by ranbir on 12/25/14.
 */

public interface UserRepository extends GraphRepository<User>{

    User        findByUsername(String username);

    @Query("match (user {username:{0}})-[:USER_MANAGES_PATIENT_CARE]-(Patient) return Patient")
    Set<Patient> getPatientsManagedByUser(String  username);

    @Query("match (user {username:{0}})-[m:USER_MANAGES_PATIENT_CARE]-(Patient) where m.careType = {1} AND m.startDate < {2} return Patient")
    Set<Patient> getPatientsManagedByUserAndCareType(String  username,CareType careType,Long now);

    @Query("match (user {username:{0}})-[m:USER_MANAGES_PATIENT_CARE]-(Patient) where m.acl = {1} AND m.startDate < {2} return Patient")
    Set<Patient> getPatientsManagedByUserAndAcl(String  username,int acl,Long now);

    @Query("match (user {username:{0}})-[m:USER_MANAGES_ASSOCIATION]-(association) where m.startDate < {1} return association")
    Set<Association> getAssociationsManagedByUser(String  username,Long now);

    @Query("MATCH (user {username:{0}})-[:MEMBER_OF*0..]-(group)<-[:HAS_ROLE]-(role) RETURN role")
    Set<Role> findAllRolesForAUserByGroups(String username);

    @Query("MATCH (user {username:{0}})-[h:USER_HAS_ADDRESS]-(address) where h.addressType = {1} return address ")
    Set<Address> findAddressByAddressType(String username,AddressType addressType);

    @Query("MATCH (user {username:{0}})-[h:USER_HAS_ADDRESS]-(address)-[:IN_LOCALITY]-(locality)-[:IN_REGION]-(region)-[:IN_COUNTRY]-(country) where h.addressType = {1} " +
            "return address.address1, address.address2, address.address3, address.address4,address.postalCode,locality.title, region.code, country.code")
    Set<FullAddress> findFullAddressByAddressType(String username,AddressType addressType);

    @Query("match (user {username:{0}})-[:USER_WORKS_AT_HEALTHCARE_FACILITY]-(HealthCareFacility) return HealthCareFacility")
    Set<HealthCareFacility> findAllFacilitiesForUser(String username, Long now);
}
