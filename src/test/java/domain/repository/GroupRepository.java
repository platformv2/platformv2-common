package domain.repository;

import com.x2biosystems.platform.common.domain.Group;
import com.x2biosystems.platform.common.domain.Role;
import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.neo4j.repository.GraphRepository;

import java.util.Set;

/**
 * Created by ranbir on 12/25/14.
 */
public interface GroupRepository extends GraphRepository<Group> {

    Group findByDisplayName(String displayName);

    /*
    This is how we should retrieve all roles for a group - this will navigate up the graph to all parent
    group nodes and return a singular set of roles to add back to the User when they join a group
    or when they leave a group

    @Query("MATCH (group {displayName:{0}})-[:MEMBER_OF*0..]-(parent)<-[:HAS_ROLE]-(role) RETURN role")

     */
    @Query("MATCH (group {displayName:{0}})<-[:HAS_ROLE]-(role) RETURN role")
    Set<Role> findAllRolesForAGroup(String displayName);

    @Query("match (user {username:{0}})-[:MEMBER_OF*0..]-(group) return group")
    Set<Group> findAllGroupsForAUser(String username);
}
