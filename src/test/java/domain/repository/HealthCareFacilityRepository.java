package domain.repository;

import com.x2biosystems.platform.common.domain.HealthCareFacility;
import org.springframework.data.neo4j.repository.GraphRepository;

/**
 * Created by ranbir on 2/18/15.
 */

public interface HealthCareFacilityRepository extends GraphRepository<HealthCareFacility> {

    HealthCareFacility findByName(String name);

}
