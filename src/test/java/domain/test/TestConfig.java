package domain.test;

import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.test.TestGraphDatabaseFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.data.neo4j.config.EnableNeo4jRepositories;
import org.springframework.data.neo4j.config.JtaTransactionManagerFactoryBean;
import org.springframework.data.neo4j.config.Neo4jConfiguration;
import org.springframework.data.neo4j.support.Neo4jTemplate;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;


import javax.annotation.Resource;
import java.io.IOException;


/**
 * Created by ranbir on 12/23/14.
 */

@Configuration
@ComponentScan(basePackages = "domain")
@EnableNeo4jRepositories(basePackages = "domain.repository")
@EnableTransactionManagement
public class TestConfig extends Neo4jConfiguration {

    @Resource
    private Environment env;

    public TestConfig() {
        setBasePackage("com.x2biosystems.platform.common.domain");
    }

    @Bean(name="transactionManager")
    public PlatformTransactionManager transactionManager() throws Exception {
        return new JtaTransactionManagerFactoryBean(graphDatabaseService()).getObject();
    }
    @Bean
    public Neo4jTemplate neo4jTemplate() throws IOException {
        return new Neo4jTemplate(graphDatabaseService());
    }
//embedded unit test database
    @Bean(destroyMethod = "shutdown")
    public GraphDatabaseService graphDatabaseService() {
        return new TestGraphDatabaseFactory().newImpermanentDatabase();
    }

}
