package domain.test.domain;

import com.x2biosystems.platform.common.domain.HealthCareFacility;
import com.x2biosystems.platform.common.domain.Patient;
import com.x2biosystems.platform.common.domain.Role;
import com.x2biosystems.platform.common.domain.User;
import domain.repository.HealthCareFacilityRepository;
import domain.repository.PatientRepository;
import domain.repository.RoleRepository;
import domain.repository.UserRepository;
import domain.test.TestConfig;
import org.joda.time.DateTime;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

import java.util.Set;

/**
 * Created by ranbir on 2/18/15.
 */

@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {TestConfig.class}, loader = AnnotationConfigContextLoader.class)
@TransactionConfiguration(transactionManager = "transactionManager", defaultRollback = true)
public class HealthCareFacilityTest {

    private TestData testData;
    private User user;
    private User    drZhivago;
    private Role parent;
    private Role    clinicianRole;
    private Patient patient;
    private DateTime start;
    private DateTime now;
    private HealthCareFacility healthCareFacility;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private HealthCareFacilityRepository healthCareFacilityRepository;

    @Autowired
    private PatientRepository patientRepository;

    @Before
    public void setup() {
        start = new DateTime().minusDays(1);//set start so it's always behind now();
        testData = new TestData();
        patient = testData.getPatient();
        drZhivago = testData.getClinician();
        clinicianRole = testData.getClinicianRole();
        roleRepository.save(clinicianRole);
        drZhivago.addRole(clinicianRole);
        userRepository.save(drZhivago);
        patientRepository.save(patient);
        healthCareFacility = testData.getFacility();
        healthCareFacilityRepository.save(healthCareFacility);

    }

    @Test
    public void getByNameTest() {
        HealthCareFacility retFacility = healthCareFacilityRepository.findByName(healthCareFacility.getName());
        Assert.assertNotNull("no facility returned",retFacility);
        Assert.assertEquals("facility name mismatch",healthCareFacility.getName(),retFacility.getName());

    }

    @Test
    public void getFacilitiesForClinicianTest() {

        drZhivago.userWorksAtHealthCareFacility(healthCareFacility,start.toDate(),clinicianRole);
        userRepository.save(drZhivago);
        Set<HealthCareFacility> retFacilities = userRepository.findAllFacilitiesForUser(drZhivago.getUsername(),start.getMillis());
        Assert.assertNotNull("faclilities came back null",retFacilities);
        Assert.assertEquals("count mismatch",1,retFacilities.size());
    }

}
