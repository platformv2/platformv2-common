package domain.test.domain;

import com.x2biosystems.platform.common.domain.Patient;
import com.x2biosystems.platform.common.domain.Ethnicity;
import com.x2biosystems.platform.common.domain.Gender;
import com.x2biosystems.platform.common.domain.Sport;
import domain.repository.PatientRepository;
import domain.repository.EthnicityRepository;
import domain.repository.SportRepository;
import domain.test.TestConfig;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Set;

import static org.junit.Assert.*;

/**
 * Created by ranbir on 12/24/14.
 */
@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {TestConfig.class}, loader = AnnotationConfigContextLoader.class)
@TransactionConfiguration(transactionManager = "transactionManager", defaultRollback = true)
public class PatientTest {

//    @Autowired
//    private Neo4jOperations template;

    private TestData testData;

    private Ethnicity c;
    private Ethnicity a;

    @Autowired
    private PatientRepository patientRepository;

    @Autowired
    private SportRepository sportRepository;

    @Autowired
    private EthnicityRepository ethnicityRepository;

    @Before
    public void setup() {
        testData = new TestData();
        c = testData.getWhite();
        a = testData.getAsianPacIslander();
        ethnicityRepository.save(a);
        ethnicityRepository.save(c);
    }

    @Test
    public void testConfig() {
        assertTrue(true);
    }

    @Test
    public void createRetrievePatient() {

        Patient rs = testData.getPatient();
        rs.setEthnicity(c);

        this.patientRepository.save(rs);
        Patient retPatient = this.patientRepository.findByName(rs.getName());
        assertNotNull("no Patient found", retPatient);
        assertEquals("name mismatch", rs.getName(), retPatient.getName());
        assertEquals("nickName mismatch",rs.getNickName(), retPatient.getNickName());
        assertNotNull("no birthdate", retPatient.getBirthDate());
        assertNotNull("no ethnicity", retPatient.getEthnicity());
        assertEquals("wrong ethnicity",rs.getEthnicity().getDisplayName(), retPatient.getEthnicity().getDisplayName());
    }

    @Test
    public void createPatientWithSport() {
        Patient rs = testData.getPatient();
        Sport sport = testData.getSport();
        rs.addSport(sport);

        patientRepository.save(rs);
        sportRepository.save(sport);
        Patient retPatient = patientRepository.findByName(rs.getName());
        assertNotNull("no Patient found", retPatient);
        assertEquals("name mismatch",rs.getName(), retPatient.getName());

        Set<Sport> sportSet = retPatient.getSports();
        assertNotNull("sport set not returned",sportSet);
        assertEquals("sport empty",1,sportSet.size());
     //   assertEquals("sport wrong","QuarterBack",positionSet.iterator().next().getPosition());
    }

    @Test
    public void lookupByGender() {
        Patient rs = testData.getPatient();
        this.patientRepository.save(rs);
        Set<Patient> malePatients = patientRepository.findByGender(Gender.MALE);
        assertNotNull(malePatients);
        assertEquals("mismatch # of male Patients",1, malePatients.size());
    }

    //save the effort of setting up a whole new test class to run one test
    @Test
    public void findAllEthnicitiesInOrderTest() {



        List<Ethnicity> ethnicities = ethnicityRepository.findAllSortByDisplayOrder();
        assertNotNull("no list returned",ethnicities);
        assertEquals("wrong list size",2,ethnicities.size());
        assertEquals("wrong ethnicity first",a.getDisplayName(),ethnicities.get(0).getDisplayName());
    }

}
