package domain.test.domain;

import com.x2biosystems.platform.common.domain.Country;
import com.x2biosystems.platform.common.domain.Language;
import com.x2biosystems.platform.common.domain.User;
import domain.repository.CountryRepository;
import domain.repository.LanguageRepository;
import domain.repository.UserRepository;
import domain.test.TestConfig;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Created by  <a href="mailto:ranbir.chawla@14zen.com">Ranbir Chawla</a> on 1/15/15.
 */

@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {TestConfig.class}, loader = AnnotationConfigContextLoader.class)
@TransactionConfiguration(transactionManager = "transactionManager", defaultRollback = true)
public class LanguageTest {

    private User user;
    private Language english;
    private Language spanish;
    private Country country;
    private TestData testData;

    @Autowired
    private LanguageRepository languageRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private CountryRepository countryRepository;

    @Before
    public void setup() {
        testData = new TestData();
        english = testData.getEnglish();
        spanish = testData.getSpanish();
        user = testData.getUser();
        country = new Country();
        country.setCode(english.getLocale().getCountry());
        country.addLanguage(english);
    }

    @Test
    public void findByLocaleIdTest() {

        english = testData.getEnglish();
        languageRepository.save(english);

        Language retLang = languageRepository.findByLocaleId(english.getLocaleId());
        assertNotNull("didn't find language",retLang);

    }

    @Test
    public void findNativeSpeaker() {

        user.setNativeLanguage(english);

        userRepository.save(user);

        User retUser = userRepository.findByUsername(user.getUsername());
        assertNotNull("didn't find user",retUser);
        assertEquals("native language mismatch",english.getLocaleId(),user.getNativeLanguage().getLocaleId());
        assertEquals("didn't have english in languages collection",1,user.getLanguages().size());

    }

    @Test
    public void countryLanguageTest() {
        countryRepository.save(country);
        languageRepository.save(english);

        Country retCountry = countryRepository.findByCode(english.getLocale().getCountry());
        assertNotNull("didn't find country",retCountry);
        assertEquals("country didn't have language in collection",1,retCountry.getLanguages().size());
        assertEquals("language didn't have country",1,english.getCountries().size());
    }
}
