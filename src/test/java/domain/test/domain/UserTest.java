package domain.test.domain;

import com.x2biosystems.platform.common.domain.Address;
import com.x2biosystems.platform.common.domain.AddressType;
import com.x2biosystems.platform.common.domain.Patient;
import com.x2biosystems.platform.common.domain.CareType;
import com.x2biosystems.platform.common.domain.DTO.FullAddress;
import com.x2biosystems.platform.common.domain.UserHasAddress;
import com.x2biosystems.platform.common.domain.Role;
import com.x2biosystems.platform.common.domain.User;
import domain.repository.AddressRepository;
import domain.repository.PatientRepository;
import domain.repository.RoleRepository;
import domain.repository.SportRepository;
import domain.repository.UserRepository;
import domain.test.TestConfig;
import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Created by ranbir on 12/25/14.
 */

@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {TestConfig.class}, loader = AnnotationConfigContextLoader.class)
@TransactionConfiguration(transactionManager = "transactionManager", defaultRollback = true)
public class UserTest {

    private TestData testData;
    private User    user;
    private User    coachTieg;
    private Role    parent;
    private Role    coachRole;
    private Patient rs;
    private DateTime start;
    private DateTime now;

    @Autowired
    private PatientRepository patientRepository;

    @Autowired
    private AddressRepository   addressRepository;

    @Autowired
    private SportRepository sportRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private UserRepository userRepository;

    @Before
    public void setup() {
        testData = new TestData();
        start = new DateTime().minusDays(1);//set start so it's always behind now();
        rs = testData.getPatient();
        user = testData.getUser();
        parent = testData.getParentGuardianRole();
        patientRepository.save(rs);
        roleRepository.save(parent);
    //all tests have this base relationship
        user.managePatientCare(rs,start.toDate(),parent);
        userRepository.save(user);
//create another user ane relationships to m assure that it doesn't interfere with first one
        coachTieg = testData.getUser2();

        Patient lm = testData.getTeenPatient();
        coachRole = testData.getCoachRole();
        patientRepository.save(lm);
        roleRepository.save(coachRole);
        coachTieg.managePatientCare(lm,start.toDate(),coachRole);
        userRepository.save(coachTieg);
        now = DateTime.now();
    }

    @Test
    public void createUserManagesPatientRelationship() {

        //now find User
        User retUser = userRepository.findByUsername(user.getUsername());
        assertNotNull("didn't find user",retUser);

        Set<Patient> patients = userRepository.getPatientsManagedByUser(user.getUsername());
        assertNotNull(patients);
        assertEquals("size mismatch",1, patients.size());
    }

    @Test
    public void createUserManagesPatientRelationshipCareType() {

        //now find User
        User retUser = userRepository.findByUsername(user.getUsername());
        assertNotNull("didn't find user",retUser);

        Set<Patient> patients = userRepository.getPatientsManagedByUserAndCareType(user.getUsername(), CareType.GUARDIAN,now.toDate().getTime());
        assertNotNull(patients);
        assertEquals("size mismatch",1, patients.size());
    }

    @Test
    public void createUserManagesPatientRelationshipACL() {


        //now find User
        User retUser = userRepository.findByUsername(user.getUsername());
        assertNotNull("didn't find user",retUser);

        Set<Patient> patients = userRepository.getPatientsManagedByUserAndAcl(user.getUsername(), parent.getDefaultACL(),now.toDate().getTime());
        assertNotNull(patients);
        assertEquals("size mismatch",1, patients.size());
    }

    @Test
    public void addCoachToRS() {
        coachTieg.managePatientCare(rs,start.toDate(),coachRole);
        userRepository.save(coachTieg);
        Set<Patient> patients = userRepository.getPatientsManagedByUserAndAcl(coachTieg.getUsername(), coachRole.getDefaultACL(),now.toDate().getTime());
        assertNotNull(patients);
        assertEquals("size mismatch",2, patients.size());
    }

    @Test
    public void retrievePatientsBeforeOurTime() {

        DateTime past = start.minusDays(3);

        Set<Patient> patients = userRepository.getPatientsManagedByUserAndAcl(user.getUsername(), parent.getDefaultACL(),past.toDate().getTime());
        assertNotNull(patients);
        assertEquals("size mismatch",0, patients.size());
    }

    @Test
    public void addAddressType() {

        Address billingAddress = testData.getFullyQualifiedAddress();
        Address shippingAddress = testData.getFullyQualifiedAddressCustom("120 BullDog Way");
        addressRepository.save(billingAddress);
        addressRepository.save(shippingAddress);

        coachTieg.addAddress(billingAddress, AddressType.BILLING);
        coachTieg.addAddress(shippingAddress,AddressType.SHIPPING);
        userRepository.save(coachTieg);
        Set<UserHasAddress> addresses = coachTieg.getAddresses();
        assertNotNull("no addresses saved", addresses);
        assertEquals("address set mismatch size",2,addresses.size());
    }

    @Test
    public void findByAddressTypeTest() {
        Address billingAddress = testData.getFullyQualifiedAddress();
        Address shippingAddress = testData.getFullyQualifiedAddressCustom("120 BullDog Way");
        addressRepository.save(billingAddress);
        addressRepository.save(shippingAddress);

        coachTieg.addAddress(billingAddress, AddressType.BILLING);
        coachTieg.addAddress(shippingAddress,AddressType.SHIPPING);
        userRepository.save(coachTieg);

        Set<Address> retAddresses = userRepository.findAddressByAddressType(coachTieg.getUsername(),AddressType.BILLING);
        assertNotNull("Found no billing address",retAddresses);
        assertEquals("retAddress size mismatch",1,retAddresses.size());
    }

    @Test
    public void findFullAddressByAddressTypeSet() {
        Address billingAddress = testData.getFullyQualifiedAddress();
        Address shippingAddress = testData.getFullyQualifiedAddressCustom("120 BullDog Way");
        addressRepository.save(billingAddress);
        addressRepository.save(shippingAddress);

        coachTieg.addAddress(billingAddress, AddressType.BILLING);
        coachTieg.addAddress(shippingAddress,AddressType.SHIPPING);
        userRepository.save(coachTieg);
        Set<FullAddress> fullAddresses = userRepository.findFullAddressByAddressType(coachTieg.getUsername(),AddressType.BILLING);
        assertNotNull("no full address",fullAddresses);
        assertEquals("full address count mismatch",1,fullAddresses.size());
        FullAddress fa = fullAddresses.iterator().next();
        assertEquals("a1 mismatch",billingAddress.getAddress1(),fa.getAddress1());
        assertEquals("a2 mismatch",billingAddress.getAddress2(),fa.getAddress2());
        assertEquals("postal code mismatch","80000",billingAddress.getPostalCode());
        assertEquals("city mismatch",billingAddress.getLocality().getTitle(),fa.getLocality());
        assertEquals("state mismatch",billingAddress.getLocality().getRegion().getCode(),fa.getRegionCode());
        assertEquals("country mismatch",billingAddress.getLocality().getRegion().getCountry().getCode(),fa.getCountryCode());
    }
}
