package domain.test.domain;

import com.x2biosystems.platform.common.domain.Address;
import com.x2biosystems.platform.common.domain.AddressType;
import com.x2biosystems.platform.common.domain.AgeLevel;
import com.x2biosystems.platform.common.domain.Patient;
import com.x2biosystems.platform.common.domain.DTO.FullAddress;
import com.x2biosystems.platform.common.domain.Role;
import com.x2biosystems.platform.common.domain.Roster;
import com.x2biosystems.platform.common.domain.Team;
import com.x2biosystems.platform.common.domain.TeamType;
import com.x2biosystems.platform.common.domain.User;
import com.x2biosystems.platform.common.domain.UserAssociatedWithTeam;
import domain.repository.AddressRepository;
import domain.repository.AgeLevelRepository;
import domain.repository.RoleRepository;
import domain.repository.RosterRepository;
import domain.repository.TeamRepository;
import domain.repository.TeamTypeRepository;
import domain.repository.UserRepository;
import domain.test.TestConfig;
import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Created by  <a href="mailto:ranbir.chawla@14zen.com">Ranbir Chawla</a> on 1/15/15.
 */

@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {TestConfig.class}, loader = AnnotationConfigContextLoader.class)
@TransactionConfiguration(transactionManager = "transactionManager", defaultRollback = true)
public class TeamTest {

    private TestData    testData;
    private DateTime    now;
    private DateTime    start;
    private Roster      roster;
    private Patient patient;
    private Team        team1;
    private User        parent;
    private Role        role;
    private TeamType   type1;
    private AgeLevel    uTen;
    private AgeLevel    uEleven;

    @Autowired
    private TeamRepository   teamRepository;

    @Autowired
    private RosterRepository rosterRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private TeamTypeRepository teamTypeRepository;

    @Autowired
    private AgeLevelRepository ageLevelRepository;
    
    @Autowired
    private AddressRepository addressRepository;

    @Before
    public void setup() {

        testData = new TestData();
        type1 = testData.getTypeOne();
        teamTypeRepository.save(type1);
        uTen = testData.getUTen();
        uEleven = testData.getUEleven();
        ageLevelRepository.save(uTen);
        ageLevelRepository.save(uEleven);
        now = DateTime.now();
        start = now.minusDays(20);
        roster = testData.getRoster();
        roster.setStartDate(start.toDate());
        roster.setEndDate(new DateTime(0).toDate());
        rosterRepository.save(roster);
        team1 = testData.getTeam1();
        team1.setTeamType(type1);
        team1.addRoster(roster);
        team1.setAgeLevel(uTen);
        parent = testData.getUser();
        role = testData.getCoachRole();
        roleRepository.save(role);
        parent.addRole(role);


        userRepository.save(parent);
        teamRepository.save(team1);
        parent.userAssociatedWithTeam(team1,start.toDate(),role);
        teamRepository.save(team1);
        userRepository.save(parent);
    }

    @Test
    public void saveAndRetrieveTeamTest() {
    //    teamRepository.save(team1);


        Team retTeam = teamRepository.findByTitle(team1.getTitle());

        assertNotNull("didn't retrieve team",retTeam);
        assertNotNull("no team type",retTeam.getTeamType());
        assertEquals("wrong type",type1.getDisplayName(),retTeam.getTeamType().getDisplayName());
        assertNotNull("no ageLevel",retTeam.getAgeLevel());
        assertEquals("age level mismatch",uTen.getDisplayName(),retTeam.getAgeLevel().getDisplayName());
    }

    @Test
    public void getCurrentRosterTest() {
  //      teamRepository.save(team1);
        Roster roster2 = new Roster();
        roster2.setStartDate(start.minusDays(50).toDate());
        roster2.setEndDate(start.minusDays(30).toDate());
        roster2.setTitle("");
        team1.addRoster(roster2);
        Long now = DateTime.now().getMillis();

        Roster retRoster = teamRepository.getCurrentRoster(team1.getTitle(),now);
        assertNotNull("Roster not found",retRoster);
        assertEquals("roster not the current roster",roster.getTitle(),retRoster.getTitle());
    }

    @Test
    public void testGetTeamsTest() {

        Set<Team> teams = teamRepository.getTeamsAssociatedWithUser(parent.getUsername(),DateTime.now().getMillis());
        assertNotNull("no teams returned",teams);
        assertEquals("teams size mis-match",1,teams.size());
    }

    @Test
    public void testGetRoleForUserAssociatedWithTeam() {
        UserAssociatedWithTeam retRole = teamRepository.getRoleForUserAssociatedWithTeam(parent.getUsername(),team1.getId(),DateTime.now().getMillis());
        assertNotNull("role not returned",retRole);
       // assertEquals("role not correct",role.getDisplayName(),retRole.getDisplayName());
    }

    @Test
    public void testAgeLevelByDisplayOrder() {

        List<AgeLevel> retAgeLevel = ageLevelRepository.findAllByDisplayOrder();
        assertNotNull("no retAgeLevel",retAgeLevel);
        assertEquals("ageLevel mismatch",uTen.getDisplayName(),retAgeLevel.get(0).getDisplayName());

    }

    @Test
    public void findByAddressTypeTest() {
        Address billingAddress = testData.getFullyQualifiedAddress();
        Address shippingAddress = testData.getFullyQualifiedAddressCustom("120 BullDog Way");
        addressRepository.save(billingAddress);
        addressRepository.save(shippingAddress);

        team1.addAddress(billingAddress, AddressType.BILLING);
        team1.addAddress(shippingAddress,AddressType.SHIPPING);
        teamRepository.save(team1);

        Set<Address> retAddresses = teamRepository.findAddressByAddressType(team1.getTitle(),AddressType.BILLING);
        assertNotNull("Found no billing address",retAddresses);
        assertEquals("retAddress size mismatch",1,retAddresses.size());
    }

    @Test
    public void findFullAddressByAddressTypeSet() {
        Address billingAddress = testData.getFullyQualifiedAddress();
        Address shippingAddress = testData.getFullyQualifiedAddressCustom("120 BullDog Way");
        addressRepository.save(billingAddress);
        addressRepository.save(shippingAddress);

        team1.addAddress(billingAddress, AddressType.BILLING);
        team1.addAddress(shippingAddress,AddressType.SHIPPING);
        teamRepository.save(team1);
        Set<FullAddress> fullAddresses = teamRepository.findFullAddressByAddressType(team1.getTitle(),AddressType.BILLING);
        assertNotNull("no full address",fullAddresses);
        assertEquals("full address count mismatch",1,fullAddresses.size());
        FullAddress fa = fullAddresses.iterator().next();
        assertEquals("a1 mismatch",billingAddress.getAddress1(),fa.getAddress1());
        assertEquals("a2 mismatch",billingAddress.getAddress2(),fa.getAddress2());
        assertEquals("postal code mismatch","80000",billingAddress.getPostalCode());
        assertEquals("city mismatch",billingAddress.getLocality().getTitle(),fa.getLocality());
        assertEquals("state mismatch",billingAddress.getLocality().getRegion().getCode(),fa.getRegionCode());
        assertEquals("country mismatch",billingAddress.getLocality().getRegion().getCountry().getCode(),fa.getCountryCode());
    }

}
