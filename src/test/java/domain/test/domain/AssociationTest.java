package domain.test.domain;

import com.x2biosystems.platform.common.domain.Address;
import com.x2biosystems.platform.common.domain.AddressType;
import com.x2biosystems.platform.common.domain.Association;
import com.x2biosystems.platform.common.domain.AssociationHasAddress;
import com.x2biosystems.platform.common.domain.Patient;
import com.x2biosystems.platform.common.domain.DTO.FullAddress;
import com.x2biosystems.platform.common.domain.Role;
import com.x2biosystems.platform.common.domain.Roster;
import com.x2biosystems.platform.common.domain.Team;
import com.x2biosystems.platform.common.domain.User;
import domain.repository.AddressRepository;
import domain.repository.AssociationRepository;
import domain.repository.RoleRepository;
import domain.repository.RosterRepository;
import domain.repository.TeamRepository;
import domain.repository.UserRepository;
import domain.test.TestConfig;
import org.joda.time.DateTime;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Created by ranbir on 2/2/15.
 */

@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {TestConfig.class}, loader = AnnotationConfigContextLoader.class)
@TransactionConfiguration(transactionManager = "transactionManager", defaultRollback = true)
public class AssociationTest {

    private TestData    testData;
    private DateTime now;
    private DateTime    start;
    private Roster roster;
    private Patient patient;
    private Team team1;
    private User associationManager;
    private Role role;
    private Association childAssociation;
    private Association parentAssociation;

    @Autowired
    private AssociationRepository associationRepository;

    @Autowired
    private TeamRepository teamRepository;

    @Autowired
    private RosterRepository rosterRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private AddressRepository addressRepository;

    @Before
    public void setup() {
        testData = new TestData();
        now = DateTime.now();
        start = now.minusDays(20);
        roster = testData.getRoster();
        roster.setStartDate(start.toDate());
        roster.setEndDate(new DateTime(0).toDate());
        rosterRepository.save(roster);
        team1 = testData.getTeam1();
        team1.addRoster(roster);
        associationManager = testData.getUser();
        role = testData.getCoachRole();
        roleRepository.save(role);
        associationManager.addRole(role);


        userRepository.save(associationManager);
        teamRepository.save(team1);

        teamRepository.save(team1);

        childAssociation = testData.getChildAssociation();
        associationRepository.save(childAssociation);
        parentAssociation = testData.getParentAssociation();
    }

    @Test
    public void findByTitleTest() {
        Association retAssociation = associationRepository.findByTitle(childAssociation.getTitle());
        Assert.assertNotNull("no association found");
        Assert.assertEquals("title mismatch",childAssociation.getTitle(),retAssociation.getTitle());
    }

    @Test
    public void getTeamsByTitleTest() {
        Team team2 = testData.getTeam2();
        teamRepository.save(team2);
        childAssociation.addTeam(team1);
        childAssociation.addTeam(team2);
        associationRepository.save(childAssociation);
        List<Team> retTeams = associationRepository.getTeamsByTitle(childAssociation.getTitle());

        Assert.assertNotNull("no team set",retTeams);
        Assert.assertEquals("team set size mismatch",2,retTeams.size());

    }

    @Test
    public void confirmTeamsAdditionTest() {
        Team team2 = testData.getTeam2();
        teamRepository.save(team2);
        childAssociation.addTeam(team1);
        childAssociation.addTeam(team2);
        associationRepository.save(childAssociation);
        Association retAssociation = associationRepository.findById(childAssociation.getId());
        Set<Team> retTeams = retAssociation.getTeams();
        Assert.assertNotNull("no team set",retTeams);
        Assert.assertEquals("team set size mismatch",2,retTeams.size());
    }

    @Test
    public void setParentAssociationTest() {
        parentAssociation.addChildAssociation(childAssociation);
        associationRepository.save(parentAssociation);
        Association retParent = childAssociation.getParentAssociation();
        Assert.assertEquals("parent title mismatch",parentAssociation.getTitle(),retParent.getTitle());
    }

    @Test
    public void usersManageAssociationTest() {
        associationManager.userManagesAssociation(childAssociation,start.toDate());
        userRepository.save(associationManager);
        List<User> userList = associationRepository.findAssociationManagers(childAssociation.getTitle(),now.getMillis());
        Assert.assertNotNull("user list is null",userList);
        Assert.assertEquals("user size mismatch",1,userList.size());
    }

    @Test
    public void addAddressType() {

        Address billingAddress = testData.getFullyQualifiedAddress();
        Address shippingAddress = testData.getFullyQualifiedAddressCustom("120 BullDog Way");
        addressRepository.save(billingAddress);
        addressRepository.save(shippingAddress);

        childAssociation.addAddress(billingAddress, AddressType.BILLING);
        childAssociation.addAddress(shippingAddress,AddressType.SHIPPING);
        associationRepository.save(childAssociation);
        Set<AssociationHasAddress> addresses = childAssociation.getAddresses();
        assertNotNull("no addresses saved", addresses);
        assertEquals("address set mismatch size",2,addresses.size());
    }

    @Test
    public void findByAddressTypeTest() {
        Address billingAddress = testData.getFullyQualifiedAddress();
        Address shippingAddress = testData.getFullyQualifiedAddressCustom("120 BullDog Way");
        addressRepository.save(billingAddress);
        addressRepository.save(shippingAddress);

        childAssociation.addAddress(billingAddress, AddressType.BILLING);
        childAssociation.addAddress(shippingAddress,AddressType.SHIPPING);
        associationRepository.save(childAssociation);

        Set<Address> retAddresses = associationRepository.findAddressByAddressType(childAssociation.getTitle(),AddressType.BILLING);
        assertNotNull("Found no billing address",retAddresses);
        assertEquals("retAddress size mismatch",1,retAddresses.size());
    }

    @Test
    public void findFullAddressByAddressTypeSet() {
        Address billingAddress = testData.getFullyQualifiedAddress();
        Address shippingAddress = testData.getFullyQualifiedAddressCustom("120 BullDog Way");
        addressRepository.save(billingAddress);
        addressRepository.save(shippingAddress);

        childAssociation.addAddress(billingAddress, AddressType.BILLING);
        childAssociation.addAddress(shippingAddress,AddressType.SHIPPING);
        associationRepository.save(childAssociation);
        Set<FullAddress> fullAddresses = associationRepository.findFullAddressByAddressType(childAssociation.getTitle(),AddressType.BILLING);
        assertNotNull("no full address",fullAddresses);
        assertEquals("full address count mismatch",1,fullAddresses.size());
        FullAddress fa = fullAddresses.iterator().next();
        assertEquals("a1 mismatch",billingAddress.getAddress1(),fa.getAddress1());
        assertEquals("a2 mismatch",billingAddress.getAddress2(),fa.getAddress2());
        assertEquals("postal code mismatch","80000",billingAddress.getPostalCode());
        assertEquals("city mismatch",billingAddress.getLocality().getTitle(),fa.getLocality());
        assertEquals("state mismatch",billingAddress.getLocality().getRegion().getCode(),fa.getRegionCode());
        assertEquals("country mismatch",billingAddress.getLocality().getRegion().getCountry().getCode(),fa.getCountryCode());
    }
}
