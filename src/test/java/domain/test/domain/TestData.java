package domain.test.domain;

import com.x2biosystems.platform.common.domain.Address;
import com.x2biosystems.platform.common.domain.AgeLevel;
import com.x2biosystems.platform.common.domain.Association;
import com.x2biosystems.platform.common.domain.HealthCareFacility;
import com.x2biosystems.platform.common.domain.Patient;
import com.x2biosystems.platform.common.domain.CareType;
import com.x2biosystems.platform.common.domain.Country;
import com.x2biosystems.platform.common.domain.Ethnicity;
import com.x2biosystems.platform.common.domain.Gender;
import com.x2biosystems.platform.common.domain.Group;
import com.x2biosystems.platform.common.domain.Language;
import com.x2biosystems.platform.common.domain.Locality;
import com.x2biosystems.platform.common.domain.PositionCategory;
import com.x2biosystems.platform.common.domain.Region;
import com.x2biosystems.platform.common.domain.Role;
import com.x2biosystems.platform.common.domain.Roster;
import com.x2biosystems.platform.common.domain.Sport;
import com.x2biosystems.platform.common.domain.Team;
import com.x2biosystems.platform.common.domain.TeamType;
import com.x2biosystems.platform.common.domain.User;
import org.joda.time.DateTime;
import org.joda.time.LocalDate;

import java.util.Locale;

/**
 * Created by ranbir on 12/24/14.
 */
public class TestData {

    public Language getEnglish() {
        Language language = new Language(Locale.ENGLISH);

        return language;
    }

    public Language getSpanish() {
        Language language = new Language(Locale.forLanguageTag("es_ES"));
        return language;
    }

    public AgeLevel getUTen() {
        AgeLevel ageLevel = new AgeLevel();
        ageLevel.setDisplayOrder(0);
        ageLevel.setDisplayName("U10");
        return  ageLevel;
    }

    public AgeLevel getUEleven() {
        AgeLevel ageLevel = new AgeLevel();
        ageLevel.setDisplayOrder(1);
        ageLevel.setDisplayName("U11");
        return ageLevel;
    }

    public Patient getPatient() {
        Patient patient = new Patient();
        patient.setName("Roger Staubach");
        patient.setName("Roger the Dodger");
        patient.setGender(Gender.MALE);

        LocalDate birthDate = new DateTime(1942,5,2,0,0).toLocalDate();
        patient.setBirthDate(birthDate.toDate());
        return patient;
    }

    public Patient getTeenPatient() {
        Patient patient = new Patient();
        patient.setName("Amazing Milennial");
        patient.setNickName("Scooter");
        LocalDate birthDate = new DateTime(1999,5,2,0,0).toLocalDate();
        patient.setGender(Gender.FEMALE);
        patient.setBirthDate(birthDate.toDate());
        return patient;
    }

    public Sport getSport() {
        Sport sport = new Sport();
        sport.setDisplayName("Football");
        sport.setDisplayOrder(0);
        return sport;
    }

    public Sport getSoccer() {
        Sport sport = new Sport();
        sport.setDisplayName("Soccer");
        sport.setDisplayOrder(1);
        return sport;
    }

    public User  getUser() {
        User user = new User();
        user.setUsername("ranbir.chawla+1@14zen.com");
        user.setFirstName("Joe");
        user.setLastName("Smith");
        user.setSalutation("Dr");
        user.setAccountNonLocked(false);
        user.setAccountNotExpired(false);
        user.setCredentialsNonExpired(false);
        user.setEnabled(true);
        return user;
    }

    public User getUser2() {
        User user = new User();
        user.setUsername("ranbir.chawla+2@14zen.com");
        user.setFirstName("Tom");
        user.setLastName("Jones");
        user.setSalutation("mr");
        user.setAccountNonLocked(false);
        user.setAccountNotExpired(false);
        user.setCredentialsNonExpired(false);
        user.setEnabled(true);
        return user;
    }

    public User getClinician() {
        User user = new User();
        user.setUsername("dr.zhivago@gmail.com");
        user.setFirstName("Ivan");
        user.setLastName("Zhivago");
        user.setSalutation("Dr");
        user.setAccountNonLocked(false);
        user.setAccountNotExpired(false);
        user.setCredentialsNonExpired(false);
        user.setEnabled(true);
        return user;
    }

    public Role getParentGuardianRole() {
        Role role = new Role();
        role.setDefaultACL(1);
        role.setCareType(CareType.GUARDIAN);
        role.setDisplayName("Parent");
        return role;
    }

    public Role getCoachRole() {
        Role role = new Role();
        role.setDefaultACL(2);
        role.setCareType(CareType.COACH);
        role.setDisplayName("Coach");
        return role;
    }

    public Role getClinicianRole() {
        Role role = new Role();
        role.setDefaultACL(3);
        role.setCareType(CareType.CLINICIAN);
        role.setDisplayName("Clinician");
        return role;
    }

    public Group getRootGroup() {
        Group group = new Group();
        group.setDisplayName("ROOT");
        return group;
    }
    /* Connected to the ROOT node - will have children */

    public Group getParentGroup() {
        Group group = new Group();
        group.setDisplayName("ParentGroup");
        group.setDefaultACL(2);

        return group;
    }
    /* also connected to ROOT - but no children for testing */
    public Group getSecondLevelGroup() {
        Group group = new Group();
        group.setDisplayName("SecondLevelGroup");
        group.setDefaultACL(2);
        return group;
    }

    public Group getCoachGroup() {
        Group group = new Group();
        group.setDisplayName("coachGroup");
        group.setDefaultACL(3);
        return group;
    }

    public Roster getRoster() {
        Roster roster = new Roster();
        roster.setTitle("2014 Season");
        return roster;
    }

    public Locality getLocality() {
        Locality locality = new Locality();
        locality.setTitle("TestCity");

        return locality;
    }

    public Region getRegion() {
        Region region = new Region();
        region.setTitle("testState");
        region.setCode("TS");
        return region;
    }

    public Country getCountry() {
        Country country = new Country();
        country.setTitle("testCountry");
        country.setCode("TC");
        return country;
    }

    public Address getFullyQualifiedAddress() {
        Address address = new Address();
        address.setAddress1("address1 st");
        address.setPostalCode("80000");
        Locality locality = getLocality();

        locality.addAddress(address);
        Region region = getRegion();
        region.addCity(locality);
        Country country = getCountry();
        country.addState(region);
        return address;
    }

    public Address getFullyQualifiedAddressCustom(String address1) {
        Address address = new Address();
        address.setAddress1(address1);
        address.setPostalCode("80000");
        Locality locality = getLocality();

        locality.addAddress(address);
        Region region = getRegion();
        region.addCity(locality);
        Country country = getCountry();
        country.addState(region);
        return address;
    }

    public Team getTeam1() {
        Team team = new Team();
        team.setTitle("Team1");
        Sport sport = getSport();
        team.setSport(sport);
        team.setStartDate(DateTime.now().toDate());
        return team;
    }

    public Team getTeam2() {
        Team team = new Team();
        team.setTitle("Team2");
        Sport sport = getSport();
        team.setSport(sport);
        team.setStartDate(DateTime.now().toDate());
        return team;
    }

    public Association getChildAssociation() {
        Association association = new Association();
        association.setTitle("Association1");
        association.setNameMascot("tigers");
        return association;
    }

    public Association getParentAssociation() {
        Association association = new Association();
        association.setTitle("ParentAssociation");
        association.setNameMascot("richdude");
        return association;
    }
    public Ethnicity getAsianPacIslander() {
        Ethnicity ethnicity = new Ethnicity();
        ethnicity.setDisplayName("Asian/Pacific Islander");
        ethnicity.setDisplayOrder(0);
        return ethnicity;
    }
    public Ethnicity getWhite() {
        Ethnicity ethnicity = new Ethnicity();
        ethnicity.setDisplayName("Caucasian");
        ethnicity.setDisplayOrder(1);
        return ethnicity;
    }

    public PositionCategory getDefense() {
        PositionCategory positionCategory = new PositionCategory();
        positionCategory.setDisplayName("Defense");
        positionCategory.setDisplayOrder(0);
        return positionCategory;
    }

    public PositionCategory getOffense() {
        PositionCategory positionCategory = new PositionCategory();
        positionCategory.setDisplayName("Offense");
        positionCategory.setDisplayOrder(1);
        return positionCategory;
    }

    public TeamType getTypeOne() {
        TeamType teamType = new TeamType();
        teamType.setDisplayOrder(0);
        teamType.setDisplayName("type1");
        return teamType;
    }

    public TeamType getTypeTwo() {
        TeamType teamType = new TeamType();
        teamType.setDisplayOrder(1);
        teamType.setDisplayName("type2");
        return teamType;
    }

    public HealthCareFacility getFacility() {
        HealthCareFacility facility = new HealthCareFacility();
        facility.setName("St. Elsewhere");
        facility.setDescription("80's style healthcare at it's finest");

        return facility;
    }
}
