package domain.test.domain;

import com.x2biosystems.platform.common.domain.Patient;
import com.x2biosystems.platform.common.domain.Roster;
import domain.repository.PatientRepository;
import domain.repository.RosterRepository;
import domain.test.TestConfig;
import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Created by ranbir on 12/25/14.
 */

@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {TestConfig.class}, loader = AnnotationConfigContextLoader.class)
@TransactionConfiguration(transactionManager = "transactionManager", defaultRollback = true)
public class RosterTest {

    private TestData testData;
    private DateTime now;
    private DateTime start;
    private Roster roster;
    private Patient patient;

    @Autowired
    private RosterRepository rosterRepository;

    @Autowired
    private PatientRepository patientRepository;


    @Before
    public void setup() {
        testData = new TestData();
        now = DateTime.now();
        start = now.minusDays(20);
        roster = testData.getRoster();
        roster.setStartDate(start.toDate());
        roster.setEndDate(new DateTime(0).toDate());
        rosterRepository.save(roster);
    }

    @Test
    public void testSaveRoster() {

        Roster roster1 = rosterRepository.findByTitle("2014 Season");
        assertNotNull("roster didn't return",roster1);
    }

    @Test
    public void testAssignPatientFindByDate() {

        patient = testData.getPatient();

        roster.playsPosition(patient,start.toDate(),"QB","18");
        patientRepository.save(patient);
        rosterRepository.save(roster);

        Set<Patient> players = rosterRepository.getPatientsByTitleAndDateRange(roster.getTitle(),start.getMillis(),now.getMillis());
        assertNotNull("players collection was null");
        assertEquals("roster size mismatch",1,players.size());
        Patient retPatient = players.iterator().next();
        assertEquals("play name correct", patient.getName(), retPatient.getName());

    }
}
