package domain.test.domain;

import com.x2biosystems.platform.common.domain.Patient;
import com.x2biosystems.platform.common.domain.PositionCategory;
import com.x2biosystems.platform.common.domain.Sport;
import domain.repository.PositionCategoryRepository;
import domain.repository.SportRepository;
import domain.test.TestConfig;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by ranbir on 2/3/15.
 */

@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {TestConfig.class}, loader = AnnotationConfigContextLoader.class)
@TransactionConfiguration(transactionManager = "transactionManager", defaultRollback = true)
public class SportTest {

    private TestData testData;
    private Sport sport1;
    private Sport sport2;
    private Patient patient;

    @Autowired
    private SportRepository sportRepository;

    @Autowired
    private PositionCategoryRepository positionCategoryRepository;


    @Before
    public void setup() {
        this.testData = new TestData();
        sport1 = testData.getSport();
        sport2 = testData.getSoccer();
        patient = testData.getPatient();
        patient.addSport(sport1);


        sportRepository.save(sport1);
        sportRepository.save(sport2);

    }

    @Test
    public void getSportsInDisplayOrderTest() {

        List<Sport> retSports = sportRepository.findAllSportsOrderByDisplayOrder();
        Assert.assertNotNull("retSports is null",retSports);
        Assert.assertEquals("size mismatch",3,retSports.size());
        Assert.assertEquals("wrong sport was first",sport1.getDisplayName(),retSports.get(0).getDisplayName());
    }

    @Test
    public void getPositionCategoriesInOrderBySportTest() {
        PositionCategory defense = testData.getDefense();
        PositionCategory offense = testData.getOffense();
        sport1.addPositionCategory(defense);
        sport1.addPositionCategory(offense);
        sportRepository.save(sport1);

        List<PositionCategory> categories = positionCategoryRepository.findAllPositionCategoryForASportByDisplayOrder(sport1.getDisplayName());
        Assert.assertNotNull("null categories",categories);
        Assert.assertEquals("returned size mismatch",2,categories.size());
        Assert.assertEquals("wrong pc",defense.getDisplayName(),categories.get(0).getDisplayName());


    }
}


