package domain.test.domain;

import com.x2biosystems.platform.common.domain.Patient;
import com.x2biosystems.platform.common.domain.Group;
import com.x2biosystems.platform.common.domain.Role;
import com.x2biosystems.platform.common.domain.User;
import domain.repository.PatientRepository;
import domain.repository.GroupRepository;
import domain.repository.RoleRepository;
import domain.repository.UserRepository;
import domain.service.UserTestService;
import domain.test.TestConfig;
import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Created by ranbir on 12/26/14.
 */

@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {TestConfig.class}, loader = AnnotationConfigContextLoader.class)
@TransactionConfiguration(transactionManager = "transactionManager", defaultRollback = true)
public class GroupTest {

    private TestData testData;
    private User user;
    private User    coachTieg;
    private Group   rootGroup;
    private Group   secondLevelGroup;
    private Group coachGroup;
    private Group parentGroup;

    private Role parentRole;
    private Role    coachRole;
    private Patient rs;
    private DateTime start;
    private DateTime now;

    @Autowired
    private UserTestService userTestService;
    @Autowired
    private GroupRepository groupRepository;

    @Autowired
    private PatientRepository patientRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private UserRepository userRepository;

    @Before
    public void setup() {
        testData = new TestData();
        start = new DateTime().minusDays(1);//set start so it's always behind now();

        secondLevelGroup = testData.getSecondLevelGroup();
        parentGroup = testData.getParentGroup();
        coachGroup = testData.getCoachGroup();

        coachRole = testData.getCoachRole();
        roleRepository.save(coachRole);
        coachGroup.addRole(coachRole);
        parentRole = testData.getParentGuardianRole();
        roleRepository.save(parentRole);
        parentGroup.addRole(parentRole);
        coachTieg = testData.getUser2();
        userRepository.save(coachTieg);

    }

    @Test
    public void addOneChildTest() {

        groupRepository.save(coachGroup);

        Group retGroup = groupRepository.findByDisplayName(coachGroup.getDisplayName());
        assertNotNull("no group returned",retGroup);

    }

    @Test
    public void findAllRolesForAUserbyGroupTest() {
        userTestService.addUserToAGroup(coachGroup,coachTieg);

        Set<Role> userRoles = userRepository.findAllRolesForAUserByGroups(coachTieg.getUsername());
        assertNotNull("userGroups null",userRoles);
        assertEquals("group size mismatch",1,userRoles.size());

    }

    @Test
    public void findAllRolesForAUserByGroupMultiTest() {
        userTestService.addUserToAGroup(coachGroup,coachTieg);
        userTestService.addUserToAGroup(parentGroup,coachTieg);
        Set<Role> userRoles = userRepository.findAllRolesForAUserByGroups(coachTieg.getUsername());
        assertNotNull("userGroups null",userRoles);
        assertEquals("group size mismatch",2,userRoles.size());
    }

}
