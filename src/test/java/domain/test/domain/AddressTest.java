package domain.test.domain;

import com.x2biosystems.platform.common.domain.Address;
import com.x2biosystems.platform.common.domain.Country;
import com.x2biosystems.platform.common.domain.Locality;
import com.x2biosystems.platform.common.domain.Region;
import domain.repository.AddressRepository;
import domain.test.TestConfig;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

import static org.junit.Assert.assertNotNull;

/**
 * Created by  <a href="mailto:ranbir.chawla@14zen.com">Ranbir Chawla</a> on 12/27/14.
 */

@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {TestConfig.class}, loader = AnnotationConfigContextLoader.class)
@TransactionConfiguration(transactionManager = "transactionManager", defaultRollback = true)
public class AddressTest {

    @Autowired
    private AddressRepository addressRepository;

    private TestData testData;
    private Address  address;

    @Before
    public void setup() {
        testData = new TestData();
        address = testData.getFullyQualifiedAddress();


    }

    @Test
    public void setupAddressTest() {
        addressRepository.save(address);
        Address retAddress = addressRepository.findOne(address.getId());
        assertNotNull("address ret null",retAddress);
        Locality retLocality = address.getLocality();
        assertNotNull("no city returned", retLocality);
        Region retRegion = retLocality.getRegion();
        assertNotNull("no state returned", retRegion);
        Country retCountry = retRegion.getCountry();
        assertNotNull("no country returned",retCountry);
    }


}
