package com.x2biosystems.platform.common.domain;

import org.springframework.data.neo4j.annotation.EndNode;
import org.springframework.data.neo4j.annotation.GraphId;
import org.springframework.data.neo4j.annotation.RelationshipEntity;
import org.springframework.data.neo4j.annotation.StartNode;

/**
 * Created by  <a href="mailto:ranbir.chawla@14zen.com">Ranbir Chawla</a> on 12/27/14.
 */

@RelationshipEntity(type="USER_HAS_ADDRESS")
public class UserHasAddress {

    @GraphId
    private Long id;

    @StartNode
    private User user;

    @EndNode
    private Address address;

    private AddressType addressType;

    public UserHasAddress() {}

    public UserHasAddress(User user, Address address, AddressType addressType) {
        this.user = user;
        this.address = address;
        this.addressType = addressType;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public AddressType getAddressType() {
        return addressType;
    }

    public void setAddressType(AddressType addressType) {
        this.addressType = addressType;
    }
}
