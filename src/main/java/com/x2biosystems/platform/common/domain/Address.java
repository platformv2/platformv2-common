package com.x2biosystems.platform.common.domain;

import org.neo4j.graphdb.Direction;
import org.springframework.data.neo4j.annotation.GraphId;
import org.springframework.data.neo4j.annotation.NodeEntity;
import org.springframework.data.neo4j.annotation.RelatedTo;
import org.springframework.data.neo4j.annotation.RelatedToVia;

/**
 * Created by  <a href="mailto:ranbir.chawla@14zen.com">Ranbir Chawla</a> on 12/27/14.
 */

@NodeEntity
public class Address {

    @GraphId
    public Long id;

    private String address1;

    private String address2;

    private String address3;

    private String address4;

    private String postalCode;


    @RelatedTo(type="LOCATED_AT",direction= Direction.OUTGOING)
    private Location location;

    @RelatedTo(type="IN_LOCALITY",elementClass = Locality.class,direction = Direction.OUTGOING)
    private Locality locality;

    @RelatedToVia(type="USER_HAS_ADDRESS",direction=Direction.INCOMING)
    private UserHasAddress users;

    @RelatedToVia(type="ASSOCIATION_HAS_ADDRESS",direction = Direction.INCOMING)
    private AssociationHasAddress associations;

    @RelatedToVia(type="TEAM_HAS_ADDRESS",direction = Direction.INCOMING)
    private TeamHasAddress teams;

    public Address() {}

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public String getAddress3() {
        return address3;
    }

    public void setAddress3(String address3) {
        this.address3 = address3;
    }

    public String getAddress4() {
        return address4;
    }

    public void setAddress4(String address4) {
        this.address4 = address4;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public Locality getLocality() {
        return locality;
    }

    public void setLocality(Locality locality) {
        this.locality = locality;
    }

    public UserHasAddress getUsers() {
        return users;
    }

    public void setUsers(UserHasAddress users) {
        this.users = users;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public AssociationHasAddress getAssociations() {
        return associations;
    }

    public void setAssociations(AssociationHasAddress associations) {
        this.associations = associations;
    }

    public TeamHasAddress getTeams() {
        return teams;
    }

    public void setTeams(TeamHasAddress teams) {
        this.teams = teams;
    }
}
