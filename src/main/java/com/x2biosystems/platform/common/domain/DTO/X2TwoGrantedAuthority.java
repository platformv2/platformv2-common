package com.x2biosystems.platform.common.domain.DTO;

import org.springframework.security.core.GrantedAuthority;

/**
 * Created by  <a href="mailto:ranbir.chawla@14zen.com">Ranbir Chawla</a> on 1/3/15.
 */

public class X2TwoGrantedAuthority implements GrantedAuthority {

    private String authority;


    @Override
    public String getAuthority() {
        return authority;
    }

    public void setAuthority(String authority) {
        this.authority = authority;
    }

    public X2TwoGrantedAuthority() {}

    public X2TwoGrantedAuthority(String authority) {
        this.authority = authority;
    }
}
