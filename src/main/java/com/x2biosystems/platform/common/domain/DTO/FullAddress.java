package com.x2biosystems.platform.common.domain.DTO;

import org.springframework.data.neo4j.annotation.QueryResult;
import org.springframework.data.neo4j.annotation.ResultColumn;

/**
 * Created by  <a href="mailto:ranbir.chawla@14zen.com">Ranbir Chawla</a> on 12/27/14.
 */

@QueryResult
public class FullAddress {

    @ResultColumn("address.address1")
    private String address1;
    @ResultColumn("address.address2")
    private String address2;
    @ResultColumn("address.address3")
    private String address3;
    @ResultColumn("address.address4")
    private String address4;
    @ResultColumn("address.postalCode")
    private String   postalCode;
    @ResultColumn("locality.title")
    private String   locality;
    @ResultColumn("region.code")
    private String regionCode;
    @ResultColumn("country.code")
    private String  countryCode;

    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public String getAddress3() {
        return address3;
    }

    public void setAddress3(String address3) {
        this.address3 = address3;
    }

    public String getAddress4() {
        return address4;
    }

    public void setAddress4(String address4) {
        this.address4 = address4;
    }

    public String getLocality() {
        return locality;
    }

    public void setLocality(String locality) {
        this.locality = locality;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getRegionCode() {
        return regionCode;
    }

    public void setRegionCode(String regionCode) {
        this.regionCode = regionCode;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }
}
