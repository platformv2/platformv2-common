package com.x2biosystems.platform.common.domain;

/**
 * Created by  <a href="mailto:ranbir.chawla@14zen.com">Ranbir Chawla</a> on 12/27/14.
 */

public enum AddressType {

    SHIPPING("SHIPPING","shipping"),
    BILLING("BILLING","billing"),
    LEGAL("LEGAL","legal"),
    GENERAL("GENERAL","general")
    ;

    private final String message;
    private final String code;

    private AddressType(final String code, final String message) {
        this.code = code;
        this.message = message;
    }

    public static AddressType fromCode(String code) {
        for (AddressType addressType : AddressType.values()) {
            if (addressType.code.equals(code)) {
                return addressType;
            }
        }
        return null;
    }

    public String toString() {
        return code + message;
    }

    public String getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }


}
