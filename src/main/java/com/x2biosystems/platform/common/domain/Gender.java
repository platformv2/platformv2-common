package com.x2biosystems.platform.common.domain;

/**
 * Created by ranbir on 12/27/14.
 */


public enum Gender {


    MALE("MALE","male"),
    FEMALE("female","female")
    ;

    private final String message;
    private final String code;

    private Gender(final String code, final String message) {
        this.code = code;
        this.message = message;
    }

    public static Gender fromCode(String code) {
        for (Gender gender : Gender.values()) {
            if (gender.code.equals(code)) {
                return gender;
            }
        }
        return null;
    }

    public String toString() {
        return code;
    }

    public String getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }
}
