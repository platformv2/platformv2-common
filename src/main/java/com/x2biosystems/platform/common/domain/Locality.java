package com.x2biosystems.platform.common.domain;

import org.neo4j.graphdb.Direction;
import org.springframework.data.neo4j.annotation.GraphId;
import org.springframework.data.neo4j.annotation.Indexed;
import org.springframework.data.neo4j.annotation.NodeEntity;
import org.springframework.data.neo4j.annotation.RelatedTo;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by  <a href="mailto:ranbir.chawla@14zen.com">Ranbir Chawla</a> on 12/27/14.
 */

@NodeEntity
public class Locality {

    @GraphId
    private Long id;

    @Indexed
    private String title;

    @RelatedTo
    private Set<Address> addressSet;

    @RelatedTo(type="IN_REGION",elementClass = Region.class,direction = Direction.OUTGOING)
    private Region region;

    public Locality() {}

    public void addAddress(Address address) {
        if(addressSet == null) {
            addressSet = new HashSet<>();
        }
        addressSet.add(address);
        address.setLocality(this);
    }

    public Set<Address> getAddressSet() {
        return addressSet;
    }

    public void setAddressSet(Set<Address> addressSet) {
        this.addressSet = addressSet;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Region getRegion() {
        return region;
    }

    public void setRegion(Region region) {
        this.region = region;
    }
}
