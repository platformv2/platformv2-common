package com.x2biosystems.platform.common.domain;


import org.springframework.data.neo4j.annotation.EndNode;
import org.springframework.data.neo4j.annotation.GraphId;
import org.springframework.data.neo4j.annotation.RelationshipEntity;
import org.springframework.data.neo4j.annotation.StartNode;

/**
 * Created by ranbir on 2/4/15.
 */

@RelationshipEntity(type="ASSOCIATION_HAS_ADDRESS")
public class AssociationHasAddress {

    @GraphId
    private Long id;

    @StartNode
    private Association association;

    @EndNode
    private Address address;

    private AddressType addressType;

    public AssociationHasAddress() {}
    public AssociationHasAddress(Association association,Address address,AddressType addressType) {
        this.association = association;
        this.address = address;
        this.addressType = addressType;

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public AddressType getAddressType() {
        return addressType;
    }

    public void setAddressType(AddressType addressType) {
        this.addressType = addressType;
    }

    public Association getAssociation() {
        return association;
    }

    public void setAssociation(Association association) {
        this.association = association;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }
}
