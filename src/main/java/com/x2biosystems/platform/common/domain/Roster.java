package com.x2biosystems.platform.common.domain;

import org.joda.time.DateTime;
import org.neo4j.graphdb.Direction;
import org.springframework.data.neo4j.annotation.Fetch;
import org.springframework.data.neo4j.annotation.GraphId;
import org.springframework.data.neo4j.annotation.GraphProperty;
import org.springframework.data.neo4j.annotation.Indexed;
import org.springframework.data.neo4j.annotation.NodeEntity;
import org.springframework.data.neo4j.annotation.RelatedTo;
import org.springframework.data.neo4j.annotation.RelatedToVia;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by ranbir on 12/25/14.
 */

@NodeEntity
public class Roster {

    @GraphId
    private Long id;

    @Indexed
    private String title;

    @Indexed
    @GraphProperty(propertyType = Long.class)
    private Date    startDate;

    @GraphProperty(propertyType = Long.class,defaultValue="0")
    private Date    endDate;

    @RelatedToVia
    private Set<Position> positions;

    @Fetch
    @RelatedTo(elementClass = Team.class,type="ASSOCIATED_TO_TEAM",direction = Direction.BOTH)
    private Team    team;

    public Roster() {
        endDate = startDate = new DateTime(0).toDate();
    }

    public Position playsPosition(Patient patient,Date startDate,String name,String PatientNumber) {
        Position position = new Position(patient,this,startDate,name,PatientNumber);
        if(positions == null ) {
            positions = new HashSet<Position>();
        }
        positions.add(position);
        return position;
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Set<Position> getPositions() {
        return positions;
    }

    public void setPositions(Set<Position> positions) {
        this.positions = positions;
    }

    public Team getTeam() {
        return team;
    }

    public void setTeam(Team team) {
        this.team = team;
    }
}
