package com.x2biosystems.platform.common.domain;

import org.neo4j.graphdb.Direction;
import org.springframework.data.neo4j.annotation.GraphId;
import org.springframework.data.neo4j.annotation.Indexed;
import org.springframework.data.neo4j.annotation.NodeEntity;
import org.springframework.data.neo4j.annotation.RelatedTo;

import java.util.HashSet;
import java.util.Locale;
import java.util.Set;

/**
 * Created by  <a href="mailto:ranbir.chawla@14zen.com">Ranbir Chawla</a> on 1/15/15.
 */

@NodeEntity
public class Language {

    @GraphId
    private Long id;

    @Indexed
    private String localeId;

    @Indexed
    private String displayName;

    private Locale locale;

    @RelatedTo(elementClass = User.class, type = "SPEAKS",direction =  Direction.INCOMING)
    private Set<User> speakers;

    @RelatedTo(elementClass = User.class, type = "SPEAKS_NATIVELY",direction =  Direction.INCOMING)
    private Set<User> nativeSpeakers;

    @RelatedTo(elementClass = Country.class,type="SPEAKS",direction = Direction.INCOMING)
    private Set<Country> countries;

    public Language() {
        this.countries = new HashSet<>();
    }

    public Language(Locale locale) {
        this.locale = locale;
        this.displayName = locale.getDisplayName(Locale.US);
        this.localeId = locale.getLanguage();
        this.countries = new HashSet<>();
    }

    public void addCountry(Country country) {
        this.countries.add(country);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLocaleId() {
        return localeId;
    }

    public void setLocaleId(String localeId) {
        this.localeId = localeId;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public Set<User> getSpeakers() {
        return speakers;
    }

    public void setSpeakers(Set<User> speakers) {
        this.speakers = speakers;
    }

    public Set<User> getNativeSpeakers() {
        return nativeSpeakers;
    }

    public void setNativeSpeakers(Set<User> nativeSpeakers) {
        this.nativeSpeakers = nativeSpeakers;
    }

    public Locale getLocale() {
        return locale;
    }

    public void setLocale(Locale locale) {
        this.locale = locale;
    }

    public Set<Country> getCountries() {
        return countries;
    }

    public void setCountries(Set<Country> countries) {
        this.countries = countries;
    }
}
