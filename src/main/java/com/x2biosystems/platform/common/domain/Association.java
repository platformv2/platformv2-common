package com.x2biosystems.platform.common.domain;

import org.neo4j.cypher.internal.compiler.v1_9.commands.expressions.Add;
import org.neo4j.graphdb.Direction;
import org.springframework.data.neo4j.annotation.Fetch;
import org.springframework.data.neo4j.annotation.GraphId;
import org.springframework.data.neo4j.annotation.Indexed;
import org.springframework.data.neo4j.annotation.NodeEntity;
import org.springframework.data.neo4j.annotation.RelatedTo;
import org.springframework.data.neo4j.annotation.RelatedToVia;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by  <a href="mailto:ranbir.chawla@14zen.com">Ranbir Chawla</a> on 1/15/15.
 */

@NodeEntity
public class Association {

    @GraphId
    private Long id;

    @Indexed
    private String title;

    private String nameMascot;

    private String logoURL;

    @Fetch
    @RelatedTo(elementClass = Association.class,type="PART_OF",direction = Direction.BOTH)
    private Association parentAssociation;

    @Fetch
    @RelatedTo(elementClass = Sport.class,type="PLAYS",direction = Direction.OUTGOING)
    private Sport sport;

    @Fetch
    @RelatedToVia(type="USER_MANAGES_ASSOCIATION",direction=Direction.INCOMING)
    private Iterable<UserManagesAssociation> associationManagers;

    @RelatedTo(elementClass = Team.class,type="PLAYS_FOR", direction = Direction.OUTGOING)
    private Set<Team> teams;

    @Fetch
    @RelatedToVia
    private Set<AssociationHasAddress> addresses;

    public Association() {
        teams = new HashSet<>();
    }

    public void addChildAssociation(Association childAssociation) {
        childAssociation.setParentAssociation(this);
    }

    public void addTeam(Team team) {
        teams.add(team);
        team.setAssociation(this);
    }

    public AssociationHasAddress addAddress(Address address,AddressType addressType) {
        AssociationHasAddress associationHasAddress = new AssociationHasAddress(this,address,addressType);
        if(addresses == null) {addresses = new HashSet<>(); }
        addresses.add(associationHasAddress);
        return associationHasAddress;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLogoURL() {
        return logoURL;
    }

    public void setLogoURL(String logoURL) {
        this.logoURL = logoURL;
    }

    public Association getParentAssociation() {
        return parentAssociation;
    }

    public void setParentAssociation(Association parentAssociation) {
        this.parentAssociation = parentAssociation;
    }

    public Sport getSport() {
        return sport;
    }

    public void setSport(Sport sport) {
        this.sport = sport;
    }

    public Iterable<UserManagesAssociation> getAssociationManagers() {
        return associationManagers;
    }

    public void setAssociationManagers(Iterable<UserManagesAssociation> associationManagers) {
        this.associationManagers = associationManagers;
    }

    public Set<Team> getTeams() {
        return teams;
    }

    public void setTeams(Set<Team> teams) {
        this.teams = teams;
    }

    public String getNameMascot() {
        return nameMascot;
    }

    public void setNameMascot(String nameMascot) {
        this.nameMascot = nameMascot;
    }

    public Set<AssociationHasAddress> getAddresses() {
        return addresses;
    }

    public void setAddresses(Set<AssociationHasAddress> addresses) {
        this.addresses = addresses;
    }
}
