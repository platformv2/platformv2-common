package com.x2biosystems.platform.common.domain;

import org.neo4j.graphdb.Direction;
import org.springframework.data.neo4j.annotation.Fetch;
import org.springframework.data.neo4j.annotation.GraphId;
import org.springframework.data.neo4j.annotation.Indexed;
import org.springframework.data.neo4j.annotation.NodeEntity;
import org.springframework.data.neo4j.annotation.RelatedTo;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by ranbir on 12/25/14.
 */

@NodeEntity
public class Group {

    @GraphId
    private Long id;

    @Indexed
    private String displayName;

    private Integer defaultACL;

    @Fetch
    @RelatedTo
    Set<User> users;

    @Fetch
    @RelatedTo(elementClass = Role.class, type = "HAS_ROLE",direction =  Direction.INCOMING)
    Set<Role> roles;

    public Group() {}

    public void addRole(Role role) {
        if(roles == null) {
            roles = new HashSet<Role>();
        }
        roles.add(role);
    }

    public void removeRole(Role role) {
        if(roles != null) {
            roles.remove(role);
        }
    }
    public Set<Role> getRoles() {
        return roles;
    }

    public void setRoles(Set<Role> roles) {
        this.roles = roles;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public Integer getDefaultACL() {
        return defaultACL;
    }

    public void setDefaultACL(Integer defaultACL) {
        this.defaultACL = defaultACL;
    }

    public Set<User> getUsers() {
        return users;
    }

    public void setUsers(Set<User> users) {
        this.users = users;
    }
}
