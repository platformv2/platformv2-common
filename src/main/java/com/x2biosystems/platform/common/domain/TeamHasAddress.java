package com.x2biosystems.platform.common.domain;

import org.springframework.data.neo4j.annotation.EndNode;
import org.springframework.data.neo4j.annotation.GraphId;
import org.springframework.data.neo4j.annotation.RelationshipEntity;
import org.springframework.data.neo4j.annotation.StartNode;

/**
 * Created by ranbir on 2/4/15.
 */
@RelationshipEntity(type="TEAM_HAS_ADDRESS")
public class TeamHasAddress {

    @GraphId
    private Long id;

    @StartNode
    private Team team;

    @EndNode
    private Address address;

    private AddressType addressType;

    public TeamHasAddress() {}

    public TeamHasAddress(Team team,Address address,AddressType addressType) {
        this.team = team;
        this.address = address;
        this.addressType = addressType;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Team getTeam() {
        return team;
    }

    public void setTeam(Team team) {
        this.team = team;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public AddressType getAddressType() {
        return addressType;
    }

    public void setAddressType(AddressType addressType) {
        this.addressType = addressType;
    }
}
