package com.x2biosystems.platform.common.domain;

/**
 * Created by  <a href="mailto:ranbir.chawla@14zen.com">Ranbir Chawla</a> on 1/4/15.
 */

public enum MeasurementSystem {

    IMPERIAL("IMPERIAL","imperial"),
    METRIC("METRIC","metric");

    private final String message;
    private final String code;

    private MeasurementSystem(final String code, final String message) {
        this.code = code;
        this.message = message;
    }

    public static MeasurementSystem fromCode(String code) {
        for (MeasurementSystem measurementSystem : MeasurementSystem.values()) {
            if (measurementSystem.code.equals(code)) {
                return measurementSystem;
            }
        }
        return null;
    }

    public String toString() {
        return code;
    }

    public String getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }
}
