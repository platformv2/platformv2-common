package com.x2biosystems.platform.common.domain;

import org.neo4j.graphdb.Direction;
import org.springframework.data.neo4j.annotation.Fetch;
import org.springframework.data.neo4j.annotation.GraphId;
import org.springframework.data.neo4j.annotation.Indexed;
import org.springframework.data.neo4j.annotation.NodeEntity;
import org.springframework.data.neo4j.annotation.RelatedTo;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by ranbir on 12/27/14.
 */

@NodeEntity
public class Country {

    @GraphId
    private Long id;

    @Indexed
    private String title;

    @Indexed
    private String code;

    public Country () {}

    @RelatedTo
    private Set<Region> regions;

    @Fetch
    @RelatedTo(elementClass = Language.class,type="SPEAKS",direction = Direction.OUTGOING)
    private Set<Language> languages;

    public void addState(Region region) {
        if(regions ==null) {
            regions = new HashSet<Region>();
        }
        regions.add(region);
        region.setCountry(this);
    }

    public void addLanguage(Language language) {
        if(languages == null) {
            languages = new HashSet<>();
        }
        languages.add(language);
        language.addCountry(this);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Set<Region> getRegions() {
        return regions;
    }

    public void setRegions(Set<Region> regions) {
        this.regions = regions;
    }

    public Set<Language> getLanguages() {
        return languages;
    }

    public void setLanguages(Set<Language> languages) {
        this.languages = languages;
    }
}
