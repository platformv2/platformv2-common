package com.x2biosystems.platform.common.domain;

import org.joda.time.DateTime;
import org.springframework.data.neo4j.annotation.EndNode;
import org.springframework.data.neo4j.annotation.GraphId;
import org.springframework.data.neo4j.annotation.GraphProperty;
import org.springframework.data.neo4j.annotation.RelationshipEntity;
import org.springframework.data.neo4j.annotation.StartNode;

import java.util.Date;

/**
 * Created by  <a href="mailto:ranbir.chawla@14zen.com">Ranbir Chawla</a> on 1/15/15.
 */

@RelationshipEntity(type="USER_MANAGES_ASSOCIATION")
public class UserManagesAssociation {

    @GraphId
    private Long id;

    @StartNode
    private User user;

    @EndNode
    private Association association;
    @GraphProperty(propertyType = Long.class)
    private Date startDate;
    @GraphProperty(propertyType = Long.class,defaultValue="0")
    private Date endDate;

    public UserManagesAssociation() {}

    public UserManagesAssociation(User user,Association association,Date startDate) {
        this.user = user;
        this.association = association;
        this.startDate = startDate;
        this.endDate = new DateTime(0).toDate();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Association getAssociation() {
        return association;
    }

    public void setAssociation(Association association) {
        this.association = association;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }
}
