package com.x2biosystems.platform.common.domain;

import org.neo4j.graphdb.Direction;
import org.springframework.data.neo4j.annotation.GraphId;
import org.springframework.data.neo4j.annotation.NodeEntity;
import org.springframework.data.neo4j.annotation.RelatedTo;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by ranbir on 12/24/14.
 */

@NodeEntity
public class Sport {

    @GraphId
    private Long id;

    private String displayName;

    private int    displayOrder;

    @RelatedTo(type = "PLAYS", direction = Direction.BOTH)
    Set<Patient> patients;

    @RelatedTo(type="POSITION_IN",direction = Direction.INCOMING)
    Set<PositionCategory> positionCategories;

    public Sport() { positionCategories = new HashSet<>(); }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public void addPatient(com.x2biosystems.platform.common.domain.Patient patient) {
        if(patients == null) {
            this.patients = new HashSet<Patient>();
        }
        this.patients.add(patient);
    }

    public void addPositionCategory(PositionCategory positionCategory) {
        positionCategory.setSport(this);
        this.positionCategories.add(positionCategory);
    }
    public int getDisplayOrder() {
        return displayOrder;
    }

    public void setDisplayOrder(int displayOrder) {
        this.displayOrder = displayOrder;
    }

    public Set<Patient> getPatients() {
        return patients;
    }

    public void setPatients(Set<Patient> patients) {
        this.patients = patients;
    }

    public Set<PositionCategory> getPositionCategories() {
        return positionCategories;
    }

    public void setPositionCategories(Set<PositionCategory> positionCategories) {
        this.positionCategories = positionCategories;
    }
}


