package com.x2biosystems.platform.common.domain;

import com.x2biosystems.platform.common.domain.DTO.X2TwoGrantedAuthority;
import org.neo4j.graphdb.Direction;
import org.springframework.data.neo4j.annotation.Fetch;
import org.springframework.data.neo4j.annotation.GraphId;
import org.springframework.data.neo4j.annotation.Indexed;
import org.springframework.data.neo4j.annotation.NodeEntity;
import org.springframework.data.neo4j.annotation.RelatedTo;
import org.springframework.data.neo4j.annotation.RelatedToVia;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


/**
 * Created by ranbir on 12/24/14.
 */

@NodeEntity
public class User implements UserDetails {

    @GraphId
    private Long id;

    private String firstName;

    private String middleName;

    private String lastName;

    private String salutation;

    /* support for Spring Security UserDetails Interface */
    @Indexed
    private String username;

    private String password;

    private boolean isAccountNotExpired;

    private boolean isAccountNonLocked;

    private boolean isCredentialsNonExpired;

    private boolean isEnabled;

    @Fetch
    @RelatedTo(elementClass = Role.class, type = "HAS_ROLE",direction =  Direction.OUTGOING)
    Set<Role> roles;

    @Fetch
    @RelatedTo(elementClass = Group.class, type="MEMBER_OF", direction = Direction.OUTGOING)
    Set<Group> groups;

    @Fetch
    @RelatedToVia
    Set<UserManagesPatientCare> Patients;

    @RelatedToVia
    Set<UserManagesAssociation> associations;

    @RelatedToVia
    Set<UserAssociatedWithTeam> teams;

    @RelatedToVia
    Set<UserWorksAtHealthCareFacility> facilities;

    @Fetch
    @RelatedToVia
    Set<UserHasAddress> addresses;

    @Fetch
    @RelatedTo(elementClass =  Language.class, type="SPEAKS_NATIVELY",direction = Direction.OUTGOING)
    private Language nativeLanguage;

    @RelatedTo(elementClass =  Language.class, type="SPEAKS",direction = Direction.OUTGOING)
    private Set<Language> languages;

    public User() {}

    public void addRole(Role role) {
        if(roles == null) roles = new HashSet<Role>();
        roles.add(role);
    }

    public void addRole(Collection<Role> roles) {
        if(roles == null) roles = new HashSet<Role>();
        roles.addAll(roles);
    }
    public void removeRole(Role role) {
        if(roles !=null) {
            roles.remove(role);
        }
    }

    public void addGroup(Group group) {
        if(groups == null) groups = new HashSet<Group>();
        groups.add(group);
        for(Role role:group.getRoles()) {
            this.addRole(role);
        }
    }

    public void deleteGroup(Group group) {
        if(groups !=null) {
            groups.remove(group);
        }
        if(roles != null) {
            for(Role role:group.getRoles()) {
                this.removeRole(role);
            }
        }
    }

    public void addLanguage(Language language) {
        if(languages == null) {
            languages = new HashSet<Language>();
        }
        languages.add(language);
    }

    public void removeLanguage(Language language) {
        if(languages != null) {
            languages.remove(language);
        }
    }

    public UserManagesPatientCare managePatientCare(Patient patient, Date startDate, Role role) {
        UserManagesPatientCare userManagesPatientCare = new UserManagesPatientCare(this, patient,startDate,role);
        if(Patients == null) {
            Patients = new HashSet<UserManagesPatientCare>();
        }
        Patients.add(userManagesPatientCare);
        return userManagesPatientCare;
    }

    public UserHasAddress addAddress(Address address,AddressType addressType) {
        UserHasAddress userHasAddress = new UserHasAddress(this,address,addressType);
        if(addresses == null) {
            addresses = new HashSet<UserHasAddress>();
        }
        addresses.add(userHasAddress);
        return userHasAddress;
    }

    public UserManagesAssociation userManagesAssociation(Association association,Date startDate) {
        UserManagesAssociation userManagesAssociation = new UserManagesAssociation(this,association,startDate);
        if(associations == null) {
            associations = new HashSet<UserManagesAssociation>();
        }
        associations.add(userManagesAssociation);
        return userManagesAssociation;
    }

    public UserAssociatedWithTeam userAssociatedWithTeam(Team team,Date startDate,Role role) {
        UserAssociatedWithTeam userAssociatedWithTeam = new UserAssociatedWithTeam(this,team,startDate,role);
        if(teams == null) {
            teams = new HashSet<>();
        }
        teams.add(userAssociatedWithTeam);
        return userAssociatedWithTeam;
    }

    public UserWorksAtHealthCareFacility userWorksAtHealthCareFacility(HealthCareFacility facility,Date startDate,Role role) {
        UserWorksAtHealthCareFacility userWorksAtHealthCareFacility = new UserWorksAtHealthCareFacility(facility,this,startDate,role);
        if(facilities == null ) { facilities = new HashSet<>();}
        facilities.add(userWorksAtHealthCareFacility);
        return userWorksAtHealthCareFacility;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getSalutation() {
        return salutation;
    }

    public void setSalutation(String salutation) {
        this.salutation = salutation;
    }

    public String getUsername() {
        return username;
    }

    @Override
    public boolean isAccountNonExpired() {
        return false;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {

        List<X2TwoGrantedAuthority> authList = new ArrayList<>();
        for(Role role:roles) {
            authList.add(new X2TwoGrantedAuthority(role.getDisplayName()));
        }
        return authList;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isAccountNotExpired() {
        return isAccountNotExpired;
    }

    public void setAccountNotExpired(boolean isAccountNotExpired) {
        this.isAccountNotExpired = isAccountNotExpired;
    }

    public boolean isAccountNonLocked() {
        return isAccountNonLocked;
    }

    public void setAccountNonLocked(boolean isAccountNonLocked) {
        this.isAccountNonLocked = isAccountNonLocked;
    }

    public boolean isCredentialsNonExpired() {
        return isCredentialsNonExpired;
    }

    public void setCredentialsNonExpired(boolean isCredentialsNonExpired) {
        this.isCredentialsNonExpired = isCredentialsNonExpired;
    }

    public boolean isEnabled() {
        return isEnabled;
    }

    public void setEnabled(boolean isEnabled) {
        this.isEnabled = isEnabled;
    }

    public Set<Role> getRoles() {
        return roles;
    }

    public void setRoles(Set<Role> roles) {
        this.roles = roles;
    }

    public Set<Group> getGroups() {
        return groups;
    }

    public void setGroups(Set<Group> groups) {
        this.groups = groups;
    }

    public Set<UserManagesPatientCare> getPatients() {
        return Patients;
    }

    public void setPatients(Set<UserManagesPatientCare> Patients) {
        this.Patients = Patients;
    }

    public Set<UserHasAddress> getAddresses() {
        return addresses;
    }

    public void setAddresses(Set<UserHasAddress> addresses) {
        this.addresses = addresses;
    }

    public Language getNativeLanguage() {
        return nativeLanguage;
    }

    public void setNativeLanguage(Language nativeLanguage) {
        this.nativeLanguage = nativeLanguage;
        this.addLanguage(nativeLanguage);
    }

    public Set<Language> getLanguages() {
        return languages;
    }

    public void setLanguages(Set<Language> languages) {
        this.languages = languages;
    }

    public Set<UserManagesAssociation> getAssociations() {
        return associations;
    }

    public void setAssociations(Set<UserManagesAssociation> associations) {
        this.associations = associations;
    }
}
