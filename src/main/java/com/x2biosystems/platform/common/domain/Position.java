package com.x2biosystems.platform.common.domain;

import org.joda.time.DateTime;
import org.springframework.data.neo4j.annotation.EndNode;
import org.springframework.data.neo4j.annotation.GraphId;
import org.springframework.data.neo4j.annotation.GraphProperty;
import org.springframework.data.neo4j.annotation.RelationshipEntity;
import org.springframework.data.neo4j.annotation.StartNode;

import java.util.Date;

/**
 * Created by ranbir on 12/25/14.
 */

@RelationshipEntity(type="PLAYS_ON")
public class Position {

    @GraphId
    private Long id;

    @StartNode
    private Patient patient;

    @EndNode
    private Roster roster;

    private String name;

    private String PatientNumber;

    @GraphProperty(propertyType = Long.class)
    private Date startDate;

    @GraphProperty(propertyType = Long.class)
    private Date endDate;

    public Position() {
    }

    public Position(Patient patient, Roster roster, Date startDate, String name, String PatientNumber) {
        this.patient = patient;
        this.roster = roster;
        this.startDate = startDate;
        this.endDate = new DateTime(0).toDate();
        this.name = name;
        this.PatientNumber = PatientNumber;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }

    public Roster getRoster() {
        return roster;
    }

    public void setRoster(Roster roster) {
        this.roster = roster;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getPatientNumber() {
        return PatientNumber;
    }

    public void setPatientNumber(String PatientNumber) {
        this.PatientNumber = PatientNumber;
    }
}
