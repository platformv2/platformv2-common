package com.x2biosystems.platform.common.domain;

import org.joda.time.DateTime;
import org.springframework.data.neo4j.annotation.EndNode;
import org.springframework.data.neo4j.annotation.GraphId;
import org.springframework.data.neo4j.annotation.GraphProperty;
import org.springframework.data.neo4j.annotation.RelationshipEntity;
import org.springframework.data.neo4j.annotation.StartNode;

import java.util.Date;

/**
 * Created by ranbir on 12/25/14.
 */

@RelationshipEntity(type="USER_MANAGES_PATIENT_CARE")
public class UserManagesPatientCare {

    @GraphId
    private Long id;

    @StartNode
    private User user;

    @EndNode
    private Patient patient;
    @GraphProperty(propertyType = Long.class)
    private Date startDate;
    @GraphProperty(propertyType = Long.class,defaultValue="0")
    private Date endDate;

    private int acl;

    private CareType careType;

    public UserManagesPatientCare() {}

    public UserManagesPatientCare(User user, Patient patient, Date startDate, Role role) {
        this.user = user;
        this.patient = patient;
        this.startDate = startDate;
        this.endDate = new DateTime(0).toDate();;
        this.careType = role.getCareType();
        this.acl = role.getDefaultACL();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public int getAcl() {
        return acl;
    }

    public void setAcl(int acl) {
        this.acl = acl;
    }

    public CareType getCareType() {
        return careType;
    }

    public void setCareType(CareType careType) {
        this.careType = careType;
    }
}
