package com.x2biosystems.platform.common.domain;

import org.joda.time.DateTime;
import org.springframework.data.neo4j.annotation.EndNode;
import org.springframework.data.neo4j.annotation.GraphId;
import org.springframework.data.neo4j.annotation.GraphProperty;
import org.springframework.data.neo4j.annotation.RelationshipEntity;
import org.springframework.data.neo4j.annotation.StartNode;

import java.util.Date;

/**
 * Created by  <a href="mailto:ranbir.chawla@14zen.com">Ranbir Chawla</a> on 1/16/15.
 */

@RelationshipEntity(type = "USER_ASSOCIATED_TO_TEAM")
public class UserAssociatedWithTeam {

    @GraphId
    private Long id;

    @StartNode
    private User user;

    @EndNode
    private Team team;

    private Role role;

    @GraphProperty(propertyType = Long.class)
    private Date startDate;
    @GraphProperty(propertyType = Long.class,defaultValue="0")
    private Date endDate;

    public UserAssociatedWithTeam() {}

    public UserAssociatedWithTeam(User user,Team team,Date startDate,Role role) {
        this.user = user;
        this.team = team;
        this.startDate = startDate;
        this.role = role;
        endDate = new DateTime(0).toDate();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Team getTeam() {
        return team;
    }

    public void setTeam(Team team) {
        this.team = team;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }
}
