package com.x2biosystems.platform.common.domain;

import org.neo4j.graphdb.Direction;
import org.springframework.data.neo4j.annotation.Fetch;
import org.springframework.data.neo4j.annotation.GraphId;
import org.springframework.data.neo4j.annotation.Indexed;
import org.springframework.data.neo4j.annotation.NodeEntity;
import org.springframework.data.neo4j.annotation.RelatedToVia;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by ranbir on 2/18/15.
 */

@NodeEntity
public class HealthCareFacility {

    @GraphId
    private Long id;

    @Indexed
    private String name;

    private String description;//long text describing who they are and what they do etc...

    // do we want logo, branding linkages etc... here?

    @Fetch
    @RelatedToVia
    private Set<HealthFacilityHasAddress> addresses;

    @RelatedToVia(type="USER_WORKS_AT_HEALTHCARE_FACILITY",direction = Direction.INCOMING)
    private Iterable<UserWorksAtHealthCareFacility> users;

/*
Need to consider GIS functionality here soon - location point and find within etc...
 */

    public HealthCareFacility() {}

    public HealthFacilityHasAddress addAddress(Address address,AddressType addressType) {
        HealthFacilityHasAddress healthFacilityHasAddress = new HealthFacilityHasAddress(this,address,addressType);
        if(addresses == null) {
            addresses = new HashSet<>(1);
        }
        addresses.add(healthFacilityHasAddress);
        return healthFacilityHasAddress;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<HealthFacilityHasAddress> getAddresses() {
        return addresses;
    }

    public void setAddresses(Set<HealthFacilityHasAddress> addresses) {
        this.addresses = addresses;
    }

    public Iterable<UserWorksAtHealthCareFacility> getUsers() {
        return users;
    }

    public void setUsers(Iterable<UserWorksAtHealthCareFacility> users) {
        this.users = users;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
