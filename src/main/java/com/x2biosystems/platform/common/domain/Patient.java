package com.x2biosystems.platform.common.domain;

import org.neo4j.graphdb.Direction;
import org.springframework.data.neo4j.annotation.Fetch;
import org.springframework.data.neo4j.annotation.GraphId;
import org.springframework.data.neo4j.annotation.GraphProperty;
import org.springframework.data.neo4j.annotation.Indexed;
import org.springframework.data.neo4j.annotation.NodeEntity;
import org.springframework.data.neo4j.annotation.RelatedTo;
import org.springframework.data.neo4j.annotation.RelatedToVia;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;


/**
 * Created by ranbir on 12/23/14.
 */

@NodeEntity
public class Patient {

    @GraphId
    private Long id;

    @Indexed
    private String name;

    private String nickName;

    @Indexed
    private Gender gender;
    @GraphProperty(propertyType = Long.class)
    private Date birthDate;

    @Fetch
    @RelatedTo(type = "HAS_ETHNICITY",direction = Direction.OUTGOING)
    private Ethnicity ethnicity;

    MeasurementSystem      measureSystem;//'METRIC'/'IMPERIAL' - matches to ENUM
    int     heightMajor;
    int     heightMinor;
    int     weightMajor;
    int     weightMinor;

    @Fetch
    @RelatedTo(elementClass = Sport.class, type = "PLAYS",direction =  Direction.BOTH)
    private Set<Sport> sports;

    @Fetch
    @RelatedToVia(type="USER_MANAGES_Patient_CARE",direction=Direction.INCOMING)
    private Iterable<UserManagesPatientCare> careManagers;

    @RelatedToVia(type="PLAYS_ON",direction=Direction.OUTGOING)
    private Iterable<Position> positions;

    public Patient() {
        this.measureSystem = MeasurementSystem.IMPERIAL;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Enum getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public Iterable<UserManagesPatientCare> getCareManagers() {
        return careManagers;
    }

    public void setCareManagers(Iterable<UserManagesPatientCare> careManagers) {
        this.careManagers = careManagers;
    }

    public Iterable<Position> getPositions() {
        return positions;
    }

    public void setPositions(Iterable<Position> positions) {
        this.positions = positions;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public Set<Sport> getSports() {
        return sports;
    }

    public void setSports(Set<Sport> sports) {
        this.sports = sports;
    }

    public void addSport(Sport sport) {
        if(this.sports == null) {
            this.sports = new HashSet<Sport>();
        }
        this.sports.add(sport);
        sport.addPatient(this);
    }

    public MeasurementSystem getMeasureSystem() {
        return measureSystem;
    }

    public void setMeasureSystem(MeasurementSystem measureSystem) {
        this.measureSystem = measureSystem;
    }

    public int getHeightMajor() {
        return heightMajor;
    }

    public void setHeightMajor(int heightMajor) {
        this.heightMajor = heightMajor;
    }

    public int getHeightMinor() {
        return heightMinor;
    }

    public void setHeightMinor(int heightMinor) {
        this.heightMinor = heightMinor;
    }

    public int getWeightMajor() {
        return weightMajor;
    }

    public void setWeightMajor(int weightMajor) {
        this.weightMajor = weightMajor;
    }

    public int getWeightMinor() {
        return weightMinor;
    }

    public void setWeightMinor(int weightMinor) {
        this.weightMinor = weightMinor;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public Ethnicity getEthnicity() {
        return ethnicity;
    }

    public void setEthnicity(Ethnicity ethnicity) {
        this.ethnicity = ethnicity;
    }
}
