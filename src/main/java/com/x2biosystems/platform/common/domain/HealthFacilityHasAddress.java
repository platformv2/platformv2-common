package com.x2biosystems.platform.common.domain;

import org.springframework.data.neo4j.annotation.EndNode;
import org.springframework.data.neo4j.annotation.GraphId;
import org.springframework.data.neo4j.annotation.RelationshipEntity;
import org.springframework.data.neo4j.annotation.StartNode;

/**
 * Created by ranbir on 2/18/15.
 */

@RelationshipEntity(type="HEALTHFACILITY_HAS_ADDRESS")
public class HealthFacilityHasAddress {

    @GraphId
    private Long id;

    @StartNode
    private HealthCareFacility healthCareFacility;

    @EndNode
    private Address address;

    private AddressType addressType;

    public HealthFacilityHasAddress() {}

    public HealthFacilityHasAddress(HealthCareFacility healthCareFacility,Address address,AddressType addressType) {

        this.healthCareFacility = healthCareFacility;
        this.address = address;
        this.addressType = addressType;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public HealthCareFacility getHealthCareFacility() {
        return healthCareFacility;
    }

    public void setHealthCareFacility(HealthCareFacility healthCareFacility) {
        this.healthCareFacility = healthCareFacility;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public AddressType getAddressType() {
        return addressType;
    }

    public void setAddressType(AddressType addressType) {
        this.addressType = addressType;
    }
}
