package com.x2biosystems.platform.common.domain;

import com.x2biosystems.platform.common.domain.DTO.X2TwoGrantedAuthority;
import org.neo4j.graphdb.Direction;
import org.springframework.data.neo4j.annotation.Fetch;
import org.springframework.data.neo4j.annotation.GraphId;
import org.springframework.data.neo4j.annotation.Indexed;
import org.springframework.data.neo4j.annotation.NodeEntity;
import org.springframework.data.neo4j.annotation.RelatedTo;
import org.springframework.security.core.GrantedAuthority;

import java.util.Set;

/**
 * Created by ranbir on 12/25/14.
 */

@NodeEntity
public class Role {

    @GraphId
    private Long id;

    @Indexed
    private String displayName;

    private Integer defaultACL;

    private CareType careType;

    @RelatedTo(elementClass = User.class, type = "HAS_ROLE",direction =  Direction.INCOMING)
    private Set<User> users;

    @Fetch
    @RelatedTo(elementClass = Group.class, type = "HAS_ROLE",direction =  Direction.OUTGOING)
    private Set<Group> groups;


    public Role() {}

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public Integer getDefaultACL() {
        return defaultACL;
    }

    public void setDefaultACL(Integer defaultACL) {
        this.defaultACL = defaultACL;
    }

    public CareType getCareType() {
        return careType;
    }

    public void setCareType(CareType careType) {
        this.careType = careType;
    }

    public Set<User> getUsers() {
        return users;
    }

    public void setUsers(Set<User> users) {
        this.users = users;
    }

    public Set<Group> getGroups() {
        return groups;
    }

    public void setGroups(Set<Group> groups) {
        this.groups = groups;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Role role = (Role) o;

        if (careType != role.careType) return false;
        if (!displayName.equals(role.displayName)) return false;
        if (id != null ? !id.equals(role.id) : role.id != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = displayName.hashCode();
        result = 31 * result + careType.hashCode();
        return result;
    }
}

