package com.x2biosystems.platform.common.domain;

/**
 * Created by ranbir on 12/25/14.
 */
public enum CareType {
    
    GUARDIAN("GUARDIAN","Guardian"),
    CLINICIAN("CLINICIAN","Clinician"),
    COACH("COACH","Coach")
    ;

    private final String message;
    private final String code;

    private CareType(final String code, final String message) {
        this.code = code;
        this.message = message;
    }

    public static CareType fromCode(String code) {
        for (CareType careType : CareType.values()) {
            if (careType.code.equals(code)) {
                return careType;
            }
        }
        return null;
    }

    public String toString() {
        return code + message;
    }

    public String getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

}

