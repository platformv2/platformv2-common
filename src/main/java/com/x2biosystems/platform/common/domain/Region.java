package com.x2biosystems.platform.common.domain;

import org.neo4j.graphdb.Direction;
import org.springframework.data.neo4j.annotation.GraphId;
import org.springframework.data.neo4j.annotation.Indexed;
import org.springframework.data.neo4j.annotation.NodeEntity;
import org.springframework.data.neo4j.annotation.RelatedTo;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by ranbir on 12/27/14.
 */

@NodeEntity
public class Region {

    @GraphId
    private Long id;

    @Indexed
    private String title;

    @Indexed
    private String code;

    @RelatedTo
    private Set<Locality> cities;

    @RelatedTo(type="IN_COUNTRY",elementClass = Country.class,direction = Direction.OUTGOING)
    private Country     country;

    public Region() {}

    public void addCity(Locality locality) {
        if(cities == null) {
            cities = new HashSet<>();
        }
        cities.add(locality);
        locality.setRegion(this);
    }
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Set<Locality> getCities() {
        return cities;
    }

    public void setCities(Set<Locality> cities) {
        this.cities = cities;
    }

    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }
}
