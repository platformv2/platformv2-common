package com.x2biosystems.platform.common.domain;

import org.joda.time.DateTime;
import org.springframework.data.neo4j.annotation.EndNode;
import org.springframework.data.neo4j.annotation.GraphId;
import org.springframework.data.neo4j.annotation.GraphProperty;
import org.springframework.data.neo4j.annotation.RelationshipEntity;
import org.springframework.data.neo4j.annotation.StartNode;

import java.util.Date;

/**
 * Created by ranbir on 2/18/15.
 */

@RelationshipEntity(type="USER_WORKS_AT_HEALTHCARE_FACILITY")
public class UserWorksAtHealthCareFacility {

    @GraphId
    private Long id;

    @StartNode
    private HealthCareFacility healthCareFacility;

    @EndNode
    private User user;

    private Role role;

    @GraphProperty(propertyType = Long.class)
    private Date startDate;
    @GraphProperty(propertyType = Long.class,defaultValue="0")
    private Date endDate;

    public UserWorksAtHealthCareFacility() {}

    public UserWorksAtHealthCareFacility(HealthCareFacility facility,User user,Date startDate, Role role) {
        this.healthCareFacility = facility;
        this.user = user;
        this.startDate = startDate;
        this.endDate = new DateTime(0).toDate();
        this.role = role;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public HealthCareFacility getHealthCareFacility() {
        return healthCareFacility;
    }

    public void setHealthCareFacility(HealthCareFacility healthCareFacility) {
        this.healthCareFacility = healthCareFacility;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }
}


