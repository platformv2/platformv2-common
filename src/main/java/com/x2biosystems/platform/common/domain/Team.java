package com.x2biosystems.platform.common.domain;

import org.neo4j.cypher.internal.compiler.v1_9.commands.expressions.Add;
import org.neo4j.graphdb.Direction;
import org.springframework.data.neo4j.annotation.Fetch;
import org.springframework.data.neo4j.annotation.GraphId;
import org.springframework.data.neo4j.annotation.GraphProperty;
import org.springframework.data.neo4j.annotation.Indexed;
import org.springframework.data.neo4j.annotation.NodeEntity;
import org.springframework.data.neo4j.annotation.RelatedTo;
import org.springframework.data.neo4j.annotation.RelatedToVia;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by  <a href="mailto:ranbir.chawla@14zen.com">Ranbir Chawla</a> on 1/15/15.
 */

@NodeEntity
public class Team {

    @GraphId
    private Long id;

    @Indexed
    private String title;

    private String logoURL;

    @Indexed
    @GraphProperty(propertyType = Long.class)
    private Date startDate;

    @GraphProperty(propertyType = Long.class,defaultValue="0")
    private Date    endDate;

    @Fetch
    @RelatedTo(type="HAS_TYPE",direction = Direction.OUTGOING)
    private TeamType teamType;

    @Fetch
    @RelatedTo(elementClass = Language.class,type="SPEAKS",direction = Direction.OUTGOING)
    private Set<Language> languages;

    @Fetch
    @RelatedTo(elementClass = Roster.class,type="ASSOCIATED_TO_TEAM",direction = Direction.BOTH)
    private Set<Roster> rosterSet;

    @Fetch
    @RelatedTo(elementClass = Sport.class,type="PLAYS",direction = Direction.OUTGOING)
    private Sport sport;

    @Fetch
    @RelatedTo(elementClass= AgeLevel.class,type="HAS_AGE_LEVEL",direction = Direction.OUTGOING)
    private AgeLevel ageLevel;

    @RelatedTo(elementClass = Association.class,type="PLAYS_FOR",direction = Direction.INCOMING)
    private Association association;

    @Fetch
    @RelatedToVia(type="USER_ASSOCIATED_TO_TEAM",direction=Direction.INCOMING)
    private Iterable<UserAssociatedWithTeam> associatedUsers;

    @Fetch
    @RelatedToVia
    private Set<TeamHasAddress> addresses;

    public Team(){
        languages = new HashSet<>();
        rosterSet = new HashSet<>();
    }

    public TeamHasAddress addAddress(Address address,AddressType addressType) {
        TeamHasAddress teamHasAddress = new TeamHasAddress(this,address,addressType);
        if(addresses == null) { addresses = new HashSet<>(); }
        addresses.add(teamHasAddress);
        return teamHasAddress;
    }

    public void addRoster(Roster roster) {
        rosterSet.add(roster);
        roster.setTeam(this);
    }

    public void removeRoster(Roster roster) {
        rosterSet.remove(roster);
    }

    public void addLanguage(Language language) {
        languages.add(language);
    }

    public void removeLanguage(Language language) {
        languages.remove(language);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLogoURL() {
        return logoURL;
    }

    public void setLogoURL(String logoURL) {
        this.logoURL = logoURL;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Set<Language> getLanguages() {
        return languages;
    }

    public void setLanguages(Set<Language> languages) {
        this.languages = languages;
    }

    public Set<Roster> getRosterSet() {
        return rosterSet;
    }

    public void setRosterSet(Set<Roster> rosterSet) {
        this.rosterSet = rosterSet;
    }

    public Sport getSport() {
        return sport;
    }

    public void setSport(Sport sport) {
        this.sport = sport;
    }

    public Association getAssociation() {
        return association;
    }

    public void setAssociation(Association association) {
        this.association = association;
    }

    public Iterable<UserAssociatedWithTeam> getAssociatedUsers() {
        return associatedUsers;
    }

    public void setAssociatedUsers(Iterable<UserAssociatedWithTeam> associatedUsers) {
        this.associatedUsers = associatedUsers;
    }


    public TeamType getTeamType() {
        return teamType;
    }

    public void setTeamType(TeamType teamType) {
        this.teamType = teamType;
    }

    public AgeLevel getAgeLevel() {
        return ageLevel;
    }

    public void setAgeLevel(AgeLevel ageLevel) {
        this.ageLevel = ageLevel;
    }
}
